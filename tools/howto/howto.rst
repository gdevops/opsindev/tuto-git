
.. index::
   pair: Git ; Howto
   pair: Clean ; History
   pair: git ; rm --cached
   pair: git ; reset --hard HEAD
   pair: Annuler ; changements
   pair: Reverse ; changes

.. _gittools_howto:

===================
Git howto
===================


Branches
==========


- https://github.com/k88hudson/git-flight-rules#branches


What is the status of my branch ?
------------------------------------

::

    git status


How to go the main branch
----------------------------

::

    git checkout main




How to delete a local and remote branch
------------------------------------------


- :ref:`del_feature_branch`


::

    git push --delete origin feature-<number>
    git branch --delete feature-<number>


How to discard local file modifications
------------------------------------------

.. seealso::

   - https://www.codementor.io/citizen428/git-tutorial-10-common-git-problems-and-how-to-fix-them-aajv0katd

Sometimes the best way to get a feel for a problem is diving in and
playing around with the code.

Unfortunately, the changes made in the process sometimes turn out to
be less than optimal, in which case reverting the file to its original
state can be the fastest and easiest solution::

    git checkout -- Gemfile # reset specified path
    git checkout -- lib bin # also works with multiple arguments



How to undo local commits
---------------------------

.. seealso::

   - https://www.codementor.io/citizen428/git-tutorial-10-common-git-problems-and-how-to-fix-them-aajv0katd


Alas, sometimes it takes us a bit longer to realize that we are on the
wrong track, and by that time one or more changes may already have been
committed locally. This is when git reset comes in handy::

    git reset HEAD~2        # undo last two commits, keep changes
    git reset --hard HEAD~2 # undo last two commits, discard changes


How to remove a file from git  (git reset, git reset --hard HEAD)
-------------------------------------------------------------------

.. seealso::

   - https://git-scm.com/docs/git-reset


::

    git reset filename


or for all files::

    git reset --hard HEAD


How to remove a file from git  (git rm --cached filename)
--------------------------------------------------------------


If the files are in the git index::

    git rm --cached filename


How to delete changes in a file from git  (git checkout -- filename)
--------------------------------------------------------------------

::

    git checkout -- filename


With the zsh shell:

::

    git checkout -- **/*.py



.. _writing_clean_history:

Writing for a clean history (git rebase)
==========================================

.. seealso::

   - :ref:`git_rebase`
   - http://www.siliconfidential.com/articles/15-seconds-to-cleaner-git-history/
   - https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History#_changing_multiple
   - https://zestedesavoir.com/tutoriels/379/refaire-lhistoire-avec-git/
   - https://nathanleclaire.com/blog/2014/09/14/dont-be-scared-of-git-rebase/
   - https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase-i
   - https://coderwall.com/p/hh-4ea/git-rebase-autosquash


How to squash my last X commits together using Git ?
-------------------------------------------------------

.. seealso::

   - :ref:`insights_squash`


Squash your git commits for a clean history
---------------------------------------------


Commit early and commit often. But when you are done with your feature,
you can combine the commits into a single commit (or a few if that makes
more sense to your history).

This allows you to get rid of minor fix commits from your git history.

- http://blog.steveklabnik.com/posts/2012-11-08-how-to-squash-commits-in-a-github-pull-request
- https://help.github.com/articles/interactive-rebase



Merging versus rebasing
=========================

.. seealso::

   - https://www.atlassian.com/git/tutorials/merging-vs-rebasing



.. _how_to_see_big_files:

How to see the big files in a git repo ?
=================================================

.. seealso::


   - :ref:`check_added_large_files`


::

    $ git rev-list --objects --all | git cat-file --batch-check='%(objecttype) %(objectname) %(objectsize) %(rest)' | awk '/^blob/ {print substr($0,6)}' | sort --numeric-sort --key=2 | cut --complement --characters=13-40 | numfmt --field=2 --to=iec-i --suffix=B --padding=7 --round=nearest

::

    76981b406428     54B README.md
    00ce6b3a9383     67B README.md
    71af4f16135f     74B typescript/typescript.rst
    923b1678531b    113B frameworks/angular/versions/7.0/7.0.rst
    0c27ee76f6f5    122B typescript/versions/3.1/3.1.rst
    25d7d6868b40    127B .gitignore
    9bc137c5d6a6    167B frameworks/angular/angular.rst
    b82fc8b50da9    172B typescript/typescript.rst
    eb9dec0b3e56    174B frameworks/angular/versions/versions.rst
    9f0c65eb24ec    178B typescript/versions/versions.rst
    4a8067c30497    197B frameworks/frameworks.rst
    c0d826815143    201B Pipfile
    539c544b94d1    211B .gitlab-ci.yml
    d84c41553ed5    211B frameworks/angular/versions/7.0/7.0.rst
    a157a70df495    221B frameworks/angular/versions/versions.rst
    1c42b7d1ef77    261B frameworks/angular/versions/versions.rst
    a785c282288e    344B frameworks/angular/angular.rst
    c252df948d7c    397B frameworks/angular/angular.rst
    efc3c9483260    515B index.rst
    427e6b8eaf08    549B requirements.txt
    c968cda42acf    549B requirements.txt
    298ea9e213e8    580B Makefile
    31824fd89ccb    598B index.rst
    f06d7c779d0b    598B index.rst
    d6d9d6d54873    761B Makefile
    27f573b87af1    787B make.bat
    bf081acb1295    883B frameworks/angular/angular.svg
    32a043e5b580  2,5KiB conf.py
    ec8c00ef69df  8,3KiB frameworks/angular/AngularJS_logo.svg.png
    177e0572915e   11KiB Pipfile.lock
    2396b9a349dc   11KiB Pipfile.lock
    399efb7822c0   20KiB humour/black_hole_node_modules.png



.. _how_to_remove_big_files:

How to remove big files from a git repo ?
==============================================


- https://rtyley.github.io/bfg-repo-cleaner/
- :ref:`check_added_large_files`
- :ref:`bfg_repo_cleaner`


Exemple
---------

::

    git clone --mirror git://example.com/some-big-repo.git


::

    java -jar ~/projects/bfg-1.13.0.jar --strip-blobs-bigger-than 1M  some-big-repo.git


::

    Deleted files
    -------------

        Filename          Git id
        ------------------------------------------------------
        db.dump         | 82a870b1 (2,0 GB), 05948004 (2,0 GB)
        db.zip          | aae0628c (307,4 MB)
        db_col.sql      | a7369b9b (60,2 MB)
        db_col.zip      | aae0628c (307,4 MB)
        tran.zip        | b81913ef (1,1 MB)


    In total, 330 object ids were changed. Full details are logged here:

        /tmp/log_col.git.bfg-report/2018-09-27/16-05-06

    BFG run is complete! When ready, run: git reflog expire --expire=now --all && git gc --prune=now --aggressive


    --
    You can rewrite history in Git - don't let Trump do it for real!
    Trump's administration has lied consistently, to make people give up on ever
    being told the truth. Don't give up: https://www.aclu.org/
    --


Setup a git hook
-------------------

- :ref:`check_added_large_files`
