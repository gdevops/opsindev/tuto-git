
.. index::
   pair: Tool; git sizer
   pair: Git ; Sizer


.. _git_sizer:

=====================
Git sizer (2018-03)
=====================

.. seealso::

   - https://github.com/github/git-sizer
   - https://blog.github.com/2018-03-05-measuring-the-many-sizes-of-a-git-repository/
   - https://www.developpez.com/actu/192311/GitHub-publie-un-nouvel-outil-nomme-git-sizer-afin-d-aider-les-mainteneurs-a-optimiser-leurs-depots/




Description
==============

Compute various size metrics for a Git repository, flagging those that
might cause problems.


Installation
=============

.. seealso:: https://github.com/github/git-sizer/#getting-started

Usage
========

By default, git-sizer outputs its results in tabular format.

For example, let's use it to analyze the Linux repository, using the
--verbose option so that all statistics are output::

    $ git-sizer --verbose
