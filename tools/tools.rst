
.. index::
   pair: Git ; Tools

.. _git_tools:

===================
Tools
===================

.. toctree::
   :maxdepth: 3

   bfg_repo_cleaner/bfg_repo_cleaner
   degit/degit
   gitmoji_cli/gitmoji_cli
   git_sizer/git_sizer
   oh_my_zsh_plugins/oh_my_zsh_plugins
   pre-commit/pre-commit
   vim_fugitive/vim_fugitive
   windows/windows
   codementor/codementor
   dangit/dangit
   howto/howto
   migration/migration
   ssh_access/ssh_access
