
.. index::
   pair: Tool ; bfg-repo-cleaner
    pair: repo ; cleaner
   ! bfg-repo-cleaner


.. _bfg_repo_cleaner:

=======================================================================================
bfg-repo-cleaner
=======================================================================================

.. seealso::

   - https://rtyley.github.io/bfg-repo-cleaner/
   - :ref:`how_to_remove_big_files`



Introduction
===============

The BFG is a simpler, faster alternative to git-filter-branch for
cleansing bad data out of your Git repository history:

- Removing Crazy Big Files
- Removing Passwords, Credentials & other Private data

The git-filter-branch command is enormously powerful and can do things
that the BFG can't - but the BFG is much better for the tasks above,
because:

- Faster : 10 - 720x faster
- Simpler : The BFG isn't particularily clever, but is focused on making
  the above tasks easy
- Beautiful : If you need to, you can use the beautiful Scala language
  to customise the BFG. Which has got to be better than Bash scripting
  at least some of the time.
