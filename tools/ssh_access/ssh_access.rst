

.. index::
   pair: Git ;  SSH
   pair: ED25519 ; SSH keys


.. _git_ssh_access:

================
Git SSH access
================

.. seealso::

   - https://docs.gitlab.com/ee/ssh/
   - https://www.digitalocean.com/community/tutorials/understanding-the-ssh-encryption-and-connection-process
   - https://linux-audit.com/using-ed25519-openssh-keys-instead-of-dsa-rsa-ecdsa/





Introduction
=============

Git is a distributed version control system, which means you can work
locally but you can also share or “push” your changes to other servers.

Before you can push your changes to a GitLab server you need a secure
communication channel for sharing information.

The SSH protocol provides this security and allows you to authenticate
to the GitLab remote server without supplying your username or password
each time.

For a more detailed explanation of how the SSH protocol works, read
this nice tutorial by DigitalOcean_


.. _DigitalOcean: https://www.digitalocean.com/community/tutorials/understanding-the-ssh-encryption-and-connection-process

ED25519 SSH keys
==================

.. seealso::

   - https://linux-audit.com/using-ed25519-openssh-keys-instead-of-dsa-rsa-ecdsa/


Following `best practices`_, you should always favor ED25519 SSH keys,
since they are more secure and have better performance over the other
types.

They were introduced in OpenSSH 6.5, so any modern OS should include
the option to create them. If for any reason your OS or the GitLab
instance you interact with doesn’t support this, you can fallback to RSA.


.. _`best practices`:  https://linux-audit.com/using-ed25519-openssh-keys-instead-of-dsa-rsa-ecdsa/


RSA SSH keys
=============

RSA keys are the most common ones and therefore the most compatible
with servers that may have an old OpenSSH version.
Use them if the GitLab server doesn’t work with ED25519 keys.

The minimum key size is 1024 bits, defaulting to 2048. If you wish to
generate a stronger RSA key pair, specify the -b flag with a higher
bit value than the default.

The old, default password encoding for SSH private keys is insecure;
it’s only a single round of an MD5 hash. Since OpenSSH version 6.5,
you should use the -o option to ssh-keygen to encode your private key
in a new, more secure format.

If you already have an RSA SSH key pair to use with GitLab, consider
upgrading it to use the more secure password encryption format by
using the following command on the private key::

    ssh-keygen -o -f ~/.ssh/id_rsa
