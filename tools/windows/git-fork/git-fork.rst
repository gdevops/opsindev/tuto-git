
.. index::
   pair: git-fork ; Git client


.. _git_fork:

===================
git-fork
===================

.. seealso::

   - https://git-fork.com/


.. contents::
  :depth: 3


Description
============

A fast an friendly git client for Mac and Windows.
