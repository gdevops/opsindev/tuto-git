
.. index::
   pair: Tortoise ; Git


.. _tortoisegit:

===================
Tortoise git
===================

.. seealso::

   - https://tortoisegit.org/
   - https://tortoisegit.org/docs/releasenotes/
