
.. index::
   pair: Tool; degit

.. _git_degit:

==============================================
degit (Straightforward project scaffolding)
==============================================

.. seealso::

   - https://github.com/Rich-Harris/degit
   - :ref:`degit`
