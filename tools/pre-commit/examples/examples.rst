.. index::
   pair: pre-commit ; examples


.. _pre_commit_examples:

====================================================================
**pre-commit** examples
====================================================================




The .pre-commit-config.yaml used for this current sphinx project
==================================================================


.. literalinclude:: ../../../.pre-commit-config.yaml
   :linenos:


https://gitlab.com/00dani/lemoncurry/blob/master/.pre-commit-config.yaml
===========================================================================


.. code-block:: yaml
   :linenos:

    repos:
      - repo: https://github.com/pre-commit/pre-commit-hooks
        rev: v1.3.0
        hooks:
          - id: autopep8-wrapper
          - id: check-byte-order-marker
          - id: check-case-conflict
          - id: check-executables-have-shebangs
          - id: check-json
          - id: check-merge-conflict
          - id: check-yaml
          - id: end-of-file-fixer
          - id: flake8
          - id: mixed-line-ending
            args:
              - --fix=lf
          - id: trailing-whitespace
      - repo: local
        hooks:
          - id: pytest
            name: Check pytest unit tests pass
            entry: pipenv run pytest
            pass_filenames: false
            language: system
            types: [python]
          - id: mypy
            name: Check mypy static types match
            entry: pipenv run mypy . --ignore-missing-imports
            pass_filenames: false
            language: system
            types: [python]



https://gitlab.com/stavros/django-webauthin/-/blob/master/.pre-commit-config.yaml
====================================================================================

.. code-block:: yaml
   :linenos:

    repos:
    - repo: https://github.com/ambv/black
      rev: 18.9b0
      hooks:
      - id: black
    - repo: https://github.com/asottile/reorder_python_imports
      rev: v1.7.0
      hooks:
      -   id: reorder-python-imports
    - repo: https://gitlab.com/pycqa/flake8
      rev: '3.7.7'
      hooks:
      - id: flake8
        args: ["--config=setup.cfg"]
        language_version: python3
    - repo: git://github.com/skorokithakis/pre-commit-mypy
      rev: v0.610
      hooks:
      - id: mypy
        args: [-s]
    - repo: local
      hooks:
      - id: gitchangelog
        language: system
        always_run: true
        pass_filenames: false
        name: Generate changelog
        entry: bash -c "gitchangelog > CHANGELOG.md"
        stages: [commit]
