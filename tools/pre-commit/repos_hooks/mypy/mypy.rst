
.. index::
   pair: pre-commit ; mypy
   ! mypy


.. _hook_mypy:

========================================================================
**mypy** repo: https://github.com/pre-commit/mirrors-mypy
========================================================================

.. seealso::

   - https://github.com/pre-commit/mirrors-mypy
   - https://github.com/pre-commit/mirrors-mypy/graphs/contributors





.pre-commit-config.yaml example
==================================

::

  - repo: https://github.com/pre-commit/mirrors-mypy
    rev: v0.800
    hooks:
      - id: mypy
        args: [--no-strict-optional, --ignore-missing-imports]
        additional_dependencies: [tokenize-rt==3.2.0]
