.. index::
   pair: hook ; reorder_python_imports

.. _reorder_python_imports_hook:

========================================================================================
**reorder_python_imports** Rewrites source to reorder python imports
========================================================================================

.. seealso::

   - https://github.com/asottile/reorder_python_imports




Goal
=====

Tool for automatically reordering python imports. Similar to isort but
uses static analysis more.
