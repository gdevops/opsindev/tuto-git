
.. index::
   pair: hook ; blacken docs

.. _blacken_docs_hook:

==================================================================
**blacken docs** repo:  https://github.com/asottile/blacken-docs
==================================================================

.. seealso::

   - https://github.com/asottile/blacken-docs




Goal
=====

Run black on python code blocks in documentation files.


::

    (tuto_git) git commit . -m "Add black docs"
    [INFO] Initializing environment for https://github.com/asottile/blacken-docs.
    [INFO] Initializing environment for https://github.com/asottile/blacken-docs:black==19.3b0.
    [INFO] Installing environment for https://github.com/asottile/blacken-docs.
    [INFO] Once installed this environment will be reused.
    [INFO] This may take a few minutes...


::

    blacken-docs.............................................................Failed
    hookid: blacken-docs

    Traceback (most recent call last):
      File "/home/pvergain/.cache/pre-commit/repo2jffwdx1/py_env-python3/bin/blacken-docs", line 10, in <module>
        sys.exit(main())
      File "/home/pvergain/.cache/pre-commit/repo2jffwdx1/py_env-python3/lib/python3.7/site-packages/blacken_docs.py", line 109, in main
        'mode': black.FileMode.AUTO_DETECT,
    AttributeError: type object 'FileMode' has no attribute 'AUTO_DETECT'


Example
========

.. literalinclude:: ../../../../.pre-commit-config.yaml
   :linenos:
