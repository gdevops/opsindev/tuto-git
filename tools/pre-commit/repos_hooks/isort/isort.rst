
.. index::
   pair: pre-commit ; isort


.. _hook_isort:

========================================================================
**isort** repo: https://github.com/asottile/seed-isort-config
========================================================================

.. seealso::

   - https://github.com/pre-commit/mirrors-isort
   - https://github.com/timothycrosley/isort
   - https://github.com/asottile/seed-isort-config





Goal
=====

This works especially well when integrated with pre-commit.

-   repo: https://github.com/asottile/seed-isort-config
    rev: v1.2.0
    hooks:
    -   id: seed-isort-config

-   repo: https://github.com/pre-commit/mirrors-isort
    rev: v4.3.4
    hooks:
    -   id: isort

In this configuration, seed-isort-config will adjust the known_third_party
section of the isort configuration before isort runs!
