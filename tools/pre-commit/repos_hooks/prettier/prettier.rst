
.. index::
   pair: pre-commit ; prettier
   ! prettier


.. _hook_prettier:

========================================================================
**prettier** repo: https://github.com/pre-commit/mirrors-prettier
========================================================================

.. seealso::

   - https://github.com/pre-commit/mirrors-prettier
   - https://github.com/prettier/prettier
   - https://github.com/pre-commit/mirrors-prettier/graphs/contributors
