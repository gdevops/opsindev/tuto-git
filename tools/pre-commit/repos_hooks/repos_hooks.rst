
.. index::
   pair: pre-commit ; hooks

.. _git_pre_commit_hooks:

==============================================================
List of **pre-commit** hooks repos
==============================================================

.. seealso::

   - https://pre-commit.com/hooks.html




Description of some repos
============================


.. toctree::
   :maxdepth: 3

   black/black
   blacken_docs/blacken_docs
   giticket/giticket
   repo_pre_commit_hooks/repo_pre_commit_hooks
   isort/isort
   mypy/mypy
   prettier/prettier
   pyqca_bandit/pyqca_bandit
   reorder_python_imports/reorder_python_imports


Ansible repos
================

.. seealso::

   - github.com/ansible/ansible-lint

- ansible-lint - This hook runs ansible-lint.


C, files repos
================

Lucas-C/pre-commit-hooks
-------------------------

.. seealso::

   - https://github.com/Lucas-C/pre-commit-hooks

- forbid-crlf - Forbid files containing CRLF end-lines to be committed
- remove-crlf - Replace CRLF end-lines by LF ones before committing
- forbid-tabs - Forbid files containing tabs to be committed
- remove-tabs - Replace tabs by whitespaces before committing
- insert-license - Insert a short license disclaimer as a header comment in source files

doublify/pre-commit-clang-format
----------------------------------

.. seealso::

   - github.com/doublify/pre-commit-clang-format

clang-format - Format files with ClangFormat.



docker, docker-compose repos
==============================

IamTheFij/docker-pre-commit
-----------------------------

.. seealso::

   - github.com/IamTheFij/docker-pre-commit

- docker-compose-check - Checks that vault files are encrypted


dockerfilelint-precommit-hooks
--------------------------------

.. seealso::

   - github.com/pryorda/dockerfilelint-precommit-hooks

- dockerfilelint - Dockerfile linter

::

    - repo: https://github.com/pryorda/dockerfilelint-precommit-hooks
      rev: v0.1.0
      hooks:
      - id: dockerfilelint
        stages: [commit]


git, lint repos
=================

gitlint
---------

.. seealso::

   - github.com/jorisroovers/gitlint
   - https://github.com/jorisroovers/gitlint/blob/master/.pre-commit-hooks.yaml

- gitlint

mattlqx/pre-commit-sign
---------------------------

.. seealso::

   - github.com/mattlqx/pre-commit-sign

- sign-commit - Hashes some fields of the commit and adds a signature
  for verification by other systems


alessandrojcm/commitlint-pre-commit-hook
-------------------------------------------

.. seealso::

   - github.com/alessandrojcm/commitlint-pre-commit-hook

- commitlint - Commitlint hook




HTML repos
===========


Lucas-C/pre-commit-hooks-lxml
-------------------------------

.. seealso::

   - https://github.com/Lucas-C/pre-commit-hooks-lxml



- forbid-html-img-without-alt-text - List every <img> tag without
  a alt= or data-ng-attr-alt= attribute
- forbid-non-std-html-attributes - Detect any non-standard attribute
  that is not prefixed with data-
- detect-missing-css-classes - Detect unused or missing CSS classes
  definitions, and abort if any of the later ones is found
- html-tags-blacklist - Forbid some HTML tags
- html-attributes-blacklist - Forbid some HTML attributes


motet-a/jinjalint
-------------------

.. seealso::

   - github.com/motet-a/jinjalint

- jinjalint - A linter which checks the indentation and the correctness
  of Jinja-like/HTML templates.


Makefile repos
=================

.. seealso::

   - https://github.com/Lucas-C/pre-commit-hooks-go


- checkmake - Makefile linter/analyze



Nodejs, dockerfile repos
=========================

.. seealso::

   -  github.com/Lucas-C/pre-commit-hooks-nodejs

- htmlhint - NodeJS HTML syntax linter (htmlhint)
- htmllint - NodeJS HTML syntax linter (htmllint)
- dockerfile_lint - Dockerfile linter
- markdown-toc - Insert a table of contents in Markdown files,
  like a README.md


.. _php_repos:

PHP repos
===========


digitalpulp/pre-commit-php
----------------------------


.. seealso::

   - https://github.com/digitalpulp/pre-commit-php


Pre-commit scripts appropriate for any PHP project.
These hooks are made as custom plugins under the pre-commit git hook
framework.

- php-lint-all - Check PHP Syntax on ALL PHP staged files with user
  friendly messages and colors
- php-lint - Runs php -l on all staged files. Exits when it hits the
  first errored file
- php-unit - Run the full php unit test. Checks which PHPUnit executable
  is available first and then runs it. Preference order is vendor/bin,
  phpunit and phpunit.phar.
- php-cs - Run php codesniffer against all staged files.
- php-cbf - Run php codesniffer against all staged files.
- php-cs-fixer - Run php coding standards fixer against all staged files.

::

    - repo: https://github.com/digitalpulp/pre-commit-php.git
      sha: 1.3.0
      hooks:
      - id: php-lint
      - id: php-unit
      - id: php-cs
        files: \.(php)$
        args: [--standard=PSR1 -p]
      - id: php-cbf
        files: \.(php)$
        args: [--standard=PSR1 -p]



Python repos
==============

pre-commit/pygrep-hooks, rst, typing
---------------------------------------

.. seealso::

   - https://github.com/pre-commit/pygrep-hooks


- python-check-blanket-noqa - Enforce that `noqa` annotations always
  occur with specific codes
- python-check-mock-methods - Prevent common mistakes of
  `assert mck.not_called()`, `assert mck.called_once_with(...)`
  and `mck.assert_called`.
- python-no-log-warn - A quick check for the deprecated `.warn()`
  method of python loggers
- python-use-type-annotations - Enforce that python3.6+ type annotations
  are used instead of type comments
- rst-backticks - Detect common mistake of using single backticks when
  writing rst


pre-commit/mirrors-mypy
------------------------

.. seealso::

   - https://github.com/pre-commit/mirrors-mypy


- mypy


PyCQA/pydocstyle
------------------

.. seealso::

   - github.com/PyCQA/pydocstyle

- pydocstyle - pydocstyle is a static analysis tool for checking
  compliance with Python docstring conventions.


.rst linter, sphinx repos
===========================

.. seealso::

   - github.com/Lucas-C/pre-commit-hooks-markup

- rst-linter - Check .rst files with the same linter used by pypi.org



Ruby repos
==============

.. seealso::

   - https://github.com/pre-commit/mirrors-ruby-lint

SASS repos
=============

.. seealso::

   - https://github.com/elidupuis/mirrors-sass-lint


.. _ruby_repos:

Security, git, ruby, markdown, shells
========================================


jumanjihouse/pre-commit-hooks
-------------------------------

.. seealso::

   - https://github.com/jumanjihouse/pre-commit-hooks


- bundler-audit - Check for vulnerable gem versions and insecure sources
- check-mailmap - Detect if an email address needs to be added to mailmap
- fasterer - Use fasterer to suggest speed improvements in Ruby
- forbid-binary - Forbid binary files from being committed
- forbid-space-in-indent - Forbid spaces in indentation
- git-check - Use git to check for conflict markers and core.whitespace errors,
  respecting .gitattributes
- git-dirty - Detect if git tree contains modified, staged, or untracked
  files
- markdownlint - Check markdown files and flag style issues
- reek - Use reek to find ruby code smells
- require-ascii - Ensure file is ascii-encoded
- rubocop - Enforce Ruby style guide with rubocop and rubocop-rspec
- shellcheck - Shell scripts conform to shellcheck
- script-must-have-extension - Non-executable shell script filename ends in .sh
- script-must-not-have-extension - Executable shell script omits the filename extension
- shfmt - Check shell style with shfmt



RUST repos
=============

.. seealso::

   - github.com/doublify/pre-commit-rust

- fmt - Format files with rustfmt.



Shell, bash repos
===================

pre-commit-shell
------------------

.. seealso::

   - github.com/detailyang/pre-commit-shell


- shell-lint - Check Shell Syntax on ALL staged files with user
  friendly messages and colors

beautysh
-----------

.. seealso::

   - github.com/bemeurer/beautysh

- beautysh - A Bash beautifier for the masses.
  https://pypi.python.org/pypi/beautysh

openstack-dev/bashate
-----------------------

.. seealso::

   - github.com/openstack-dev/bashate

- bashate - This hook runs bashate for linting shell scripts


Terraform, prometheus repos
============================

kintoandar/pre-commit
-----------------------

.. seealso::

   - github.com/kintoandar/pre-commit

- terraform_fmt - Formats terraform scripts into the correct checkstyle
- terraform_validate - Validates terraform scripts syntax
- prometheus_check_rules - Validates prometheus rules
- prometheus_check_config - Validates prometheus configuration


fortman/pre-commit-prometheus
-------------------------------

.. seealso::

   - github.com/fortman/pre-commit-prometheus

- check-config - Check prometheus config files
- check-rules - Check prometheus rule files

Text, regular expressions
===============================

.. seealso::

   - github.com/mattlqx/pre-commit-search-and-replace

- search-and-replace - Search and replace strings


Typescript repos
===================

.. seealso::

   - https://github.com/awebdeveloper/pre-commit-tslint

- tslint


::

    -   repo: git://github.com/awebdeveloper/pre-commit-tslint/
            sha: ''  # Use the sha or tag you want to point at
            hooks:
            -   id: tslint
                additional_dependencies: ['tslint@5.0.0']
