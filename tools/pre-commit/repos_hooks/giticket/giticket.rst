
.. index::
   pair: pre-commit ; giticket


.. _giticket:

==============================================================
**giticket**  repo: https://github.com/milin/giticket
==============================================================

.. seealso::

   - https://github.com/milin/giticket




Features
==========

This hook saves developers time by prepending ticket numbers to commit-msgs.

For this to work the following two conditions must be met:
   - The ticket format regex specified must match, if the regex is passed in.
   - The branch name format must be <ticket number>_<rest of the branch name>

For e.g. if you name your branch ``JIRA-1234_awesome_feature`` and commit
``Fix some bug``, the commit will be updated to ``JIRA-1234 Fix some bug``.

Pass ``--regex=`` or update ``args: [--regex=<custom regex>]`` in your .yaml
file if you have custom ticket regex.

By default its ``[A-Z]+-\d+``.


It is best used along with pre-commit.

You can use it along with pre-commit by adding the following hook in your
``.pre-commit-config.yaml`` file.

::

    repos:
    - repo:  https://github.com/milin/giticket
      rev: '8814c81'
      hooks:
      - id:  giticket
        args: ['--regex=PROJ-[0-9]']  # Optional
