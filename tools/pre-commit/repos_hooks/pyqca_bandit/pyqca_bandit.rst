
.. index::
   pair: hook ; PyCQA/bandit

.. _pyqca_bandit:

===========================================================
**PyCQA/bandit** repo: https://github.com/PyCQA/bandit
===========================================================

.. seealso::

   - https://github.com/PyCQA/bandit



Goal
=====

Bandit is a tool designed to find common security issues in Python code.


::

    repos:
    -   repo: https://github.com/PyCQA/bandit
        rev: '' # Update me!
        hooks:
        - id: bandit
