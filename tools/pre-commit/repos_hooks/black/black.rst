
.. index::
   pair: hook ; black
   pair: repo ; https://github.com/ambv/black
   pair: Python ; code formater


.. _python_black_hook:
.. _black_hook:

===========================================================
**black** repo: https://github.com/ambv/black
===========================================================

.. seealso::

   - https://github.com/ambv/black
   - https://black.readthedocs.io/en/stable/




Goal
=====

**black** the uncompromising Python code formatter

By using Black, you agree to cede control over minutiae of hand-formatting.

In return, Black gives you speed, determinism, and freedom from
pycodestyle nagging about formatting.

You will save time and mental energy for more important matters.

Black makes code review faster by producing the smallest diffs possible.

Blackened code looks the same regardless of the project you’re reading.
Formatting becomes transparent after a while and you can focus on the
content instead.


Example
========

.. literalinclude:: ../../../../.pre-commit-config.yaml
   :linenos:


::

    make check_all

::

    pre-commit run --all-files
    Trim Trailing Whitespace.................................................Passed
    Fix End of Files.........................................................Passed
    Check Yaml...............................................................Passed
    Check for added large files..............................................Passed
    black....................................................................Failed
    hookid: black

    Files were modified by this hook. Additional output:

    reformatted /home/pvergain/projects/tuto_git/conf.py
    reformatted /home/pvergain/projects/tuto_git/tools/pre-commit/hooks/pre-commit
    All done! ✨ 🍰 ✨
    2 files reformatted.
