
.. index::
   pair: hook ; forbid-new-submodules
   pair: git ; forbid-new-submodules

.. _forbid_new_submodules:

=========================================================================
**forbid-new-submodules** prevent addition of new git submodules
=========================================================================



Goal
=====

Prevent addition of new git submodules.


Example
========

.. literalinclude:: ../../../../../.pre-commit-config.yaml
   :linenos:
