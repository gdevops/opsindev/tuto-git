
.. index::
   pair: hook; trailing-whitespace


.. _trailing_whitespace:

==============================================================
**trailing-whitespace** trims trailing whitespace
==============================================================





Goal
=====

Markdown linebreak trailing spaces preserved for `.md` and`.markdown`;

Use:

- `args: ['--markdown-linebreak-ext=txt,text']` to add other extensions,
- `args: ['--markdown-linebreak-ext=*']` to preserve them for all files,
- or `args: ['--no-markdown-linebreak-ext']` to disable and always trim.


Example
========

.. literalinclude:: ../../../../../.pre-commit-config.yaml
   :linenos:
