
.. index::
   pair: https://github.com/pre-commit/pre-commit-hooks ; repo

.. _repo_pre_commit_hooks:

===================================================================
**hooks** in repo: https://github.com/pre-commit/pre-commit-hooks
===================================================================

.. seealso::

   - https://pre-commit.com/hooks.html
   - https://github.com/pre-commit/pre-commit-hooks


.. toctree::
   :maxdepth: 3

   check-added-large-files/check-added-large-files
   check-executables-have-shebangs/check-executables-have-shebangs
   check-yaml/check-yaml
   check-json/check-json
   end-of-file-fixer/end-of-file-fixer
   fix-encoding-pragma/fix-encoding-pragma
   forbid-new-submodules/forbid-new-submodules
   mixed-line-ending/mixed-line-ending
   no-commit-to-branch/no-commit-to-branch
   pretty-format-json/pretty-format-json
   trailing-whitespace/trailing-whitespace
   other/other
