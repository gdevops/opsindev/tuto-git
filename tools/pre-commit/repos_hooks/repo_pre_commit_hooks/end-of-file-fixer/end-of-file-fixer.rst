
.. index::
   pair: hook ; end-of-file-fixer


.. _end_of_file_fixer:

===========================================================================
**end-of-file-fixer** makes sure files end in a newline and only a newline
===========================================================================




Goal
=====

Makes sure files end in a newline and only a newline.

Example
========

.. literalinclude:: ../../../../../.pre-commit-config.yaml
   :linenos:
