
.. index::
   pair: hook ; mixed-line-ending


.. _mixed_line_ending:

=================================================================
**mixed-line-ending** replaces or checks mixed line ending
=================================================================



Goal
=====

Replaces or checks mixed line ending.

- `--fix={auto,crlf,lf,no}`
    - `auto` - Replaces automatically the most frequent line ending.
      This is the default argument.
    - `crlf`, `lf` - Forces to replace line ending by respectively CRLF and LF.
    - `no` - Checks if there is any mixed line ending without modifying any file.


Example
========

.. literalinclude:: ../../../../../.pre-commit-config.yaml
   :linenos:
