
.. index::
   pair: hook; fix-encoding-pragma


.. _fix_encoding_pragma:

=======================================================================================================
**fix-encoding-pragma** remove the coding pragma in a **python3-only** codebase.
=======================================================================================================




Goal
=====

fix-encoding-pragma

To remove the coding pragma pass::

     `--remove`

in a **python3-only** codebase.


Example
========

.. literalinclude:: ../../../../../.pre-commit-config.yaml
   :linenos:
