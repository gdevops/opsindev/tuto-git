
.. index::
   pair: hook; check-executables-have-shebangs


.. _check_executables_have_shebangs:

=======================================================================================================
**check-executables-have-shebangs** checks that non-binary executables have a proper shebang.
=======================================================================================================




Goal
=====

Checks that non-binary executables have a proper shebang.


Example
========

.. literalinclude:: ../../../../../.pre-commit-config.yaml
   :linenos:
