
.. index::
   pair: hook ; check-added-large-files


.. _check_added_large_files:

=======================================================================
**check-added-large-files** prevent giant files from being committed
=======================================================================

.. seealso::

   - :ref:`how_to_see_big_files`
   - :ref:`how_to_remove_big_files`




Goal
=====

- Specify what is "too large" with `args: ['--maxkb=123']` (default=500kB).
- If `git-lfs` is installed, lfs files will be skipped
  (requires `git-lfs>=2.2.1`)


Example
========

.. literalinclude:: ../../../../../.pre-commit-config.yaml
   :linenos:
