


.. _git_pre_commit_hooks_other:

==============================================================
Some out-of-the-box hooks for pre-commit.
==============================================================

.. seealso::

   - https://pre-commit.com/hooks.html
   - https://github.com/pre-commit/pre-commit-hooks



Other hooks
===============

- `autopep8-wrapper` - Runs autopep8 over python source.
    - Ignore PEP 8 violation types with `args: ['-i', '--ignore=E000,...']` or
      through configuration of the `[pycodestyle]` section in
      setup.cfg / tox.ini.
- `check-ast` - Simply check whether files parse as valid python.
- `check-builtin-literals` - Require literal syntax when initializing empty or zero Python builtin types.
    - Allows calling constructors with positional arguments (e.g., `list('abc')`).
    - Allows calling constructors from the `builtins` (`__builtin__`) namespace (`builtins.list()`).
    - Ignore this requirement for specific builtin types with `--ignore=type1,type2,…`.
    - Forbid `dict` keyword syntax with `--no-allow-dict-kwargs`.
- `check-byte-order-marker` - Forbid files which have a UTF-8 byte-order marker
- `check-case-conflict` - Check for files with names that would conflict on a
  case-insensitive filesystem like MacOS HFS+ or Windows FAT.
- `check-docstring-first` - Checks for a common error of placing code before
  the docstring.
- `check-merge-conflict` - Check for files that contain merge conflict strings.
- `check-symlinks` - Checks for symlinks which do not point to anything.
- `check-vcs-permalinks` - Ensures that links to vcs websites are permalinks.
- `check-xml` - Attempts to load all xml files to verify syntax.
- `debug-statements` - Check for debugger imports and py37+ `breakpoint()`
  calls in python source.
- `detect-aws-credentials` - Checks for the existence of AWS secrets that you
  have set up with the AWS CLI.
  The following arguments are available:
- `--credentials-file` - additional AWS CLI style configuration file in a
  non-standard location to fetch configured credentials from. Can be repeated
  multiple times.
- `detect-private-key` - Checks for the existence of private keys.
- `double-quote-string-fixer` - This hook replaces double quoted strings
  with single quoted strings.
- `file-contents-sorter` - Sort the lines in specified files (defaults to alphabetical).
   You must provide list of target files as input to it.
   Note that this hook WILL remove blank lines and does NOT respect any comments.
- `flake8` - Run flake8 on your python files.
- `name-tests-test` - Assert that files in tests/ end in `_test.py`.
    - Use `args: ['--django']` to match `test*.py` instead.
- `pyflakes` - Run pyflakes on your python files.
- `requirements-txt-fixer` - Sorts entries in requirements.txt and removes incorrect entry for `pkg-resources==0.0.0`
- `sort-simple-yaml` - Sorts simple YAML files which consist only of
  top-level keys, preserving comments and blocks.
