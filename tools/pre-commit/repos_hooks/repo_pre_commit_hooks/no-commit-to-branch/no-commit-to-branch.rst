
.. index::
   pair: hook ; no-commit-to-branch

.. _no_commit_to_branch:

=========================================================================
**no-commit-to-branch** protect specific branches from direct checkins
=========================================================================



Goal
=====

Protect specific branches from direct checkins.

Use `args: [--branch, staging, --branch, master]` to set the branch.
`master` is the default if no argument is set.

- `-b` / `--branch` may be specified multiple times to protect multiple
  branches.


::

    Trim Trailing Whitespace.................................................Passed
    Fix End of Files.........................................................Passed
    Check Yaml...............................................................Passed
    Check JSON...........................................(no files to check)Skipped
    Fix python encoding pragma...........................(no files to check)Skipped
    Mixed line ending........................................................Passed
    Pretty format JSON...................................(no files to check)Skipped
    Check for added large files..............................................Passed
    Don't commit to branch...................................................Failed
    black................................................(no files to check)Skipped



Example
========

.. literalinclude:: ../../../../../.pre-commit-config.yaml
   :linenos:
