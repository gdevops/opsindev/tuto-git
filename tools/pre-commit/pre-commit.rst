
.. index::
   pair: Tool ; pre-commit
   pair: pre-commit ; installation
   pair: Python ; pre-commit
   ! pre-commit

.. _python_pre_commit_tool:

======================================================================================================
**pre-commit** : a Python framework for managing and maintaining multi-language pre-commit hooks
======================================================================================================

.. seealso::

   - :ref:`git_hooks`
   - https://pre-commit.com/
   - https://pre-commit.com/hooks.html
   - https://github.com/pre-commit/pre-commit
   - :ref:`project_pre_commit`
   - :ref:`ides_pre_commit_tool`



Introduction
===============

Git hook scripts are useful for identifying simple issues before submission
to code review.

We run our hooks on every commit to automatically point out issues in
code such as missing semicolons, :ref:`trailing whitespace <trailing_whitespace>`,
and debug statements.

By pointing these issues out before code review, this allows a code
reviewer to focus on the architecture of a change while not wasting
time with trivial style nitpicks.

As we created more libraries and projects we recognized that sharing
our pre-commit hooks across projects is painful. We copied and pasted
unwieldy bash scripts from project to project and had to manually change
the hooks to work for different project structures.

We believe that you should always use the best industry standard linters.

Some of the best linters are written in languages that you do not use
in your project or have installed on your machine. For example scss-lint
is a linter for SCSS written in Ruby. If you’re writing a project in
node you should be able to use scss-lint as a pre-commit hook without
adding a Gemfile to your project or understanding how to get scss-lint
installed.

We built **pre-commit to solve our hook issues**.

It is a multi-language package manager for pre-commit hooks.

You specify a list of hooks you want and pre-commit manages the
installation and execution of any hook written in any language before
every commit.

pre-commit is specifically designed to not require root access.

If one of your developers doesn’t have node installed but modifies a
JavaScript file, pre-commit automatically handles downloading and
building node to run eslint without root.


.. _git_install_pre_commit:

**pre-commit** installation, .pre-commit-config.yaml
=======================================================

.. seealso::

   - :ref:`python_pyenv_installation`

::

    pip install pre-commit


Once you have pre-commit installed, adding pre-commit plugins to your
project is done with the .pre-commit-config.yaml configuration file.

Add a file called .pre-commit-config.yaml to the root of your project.

The pre-commit config file describes what repositories and hooks are installed.


.. literalinclude:: ../../.pre-commit-config.yaml
   :linenos:


pre-commit install
---------------------

::

    pre-commit install

::

    pre-commit installed at .git/hooks/pre-commit



pre-commit install-hooks
--------------------------

If there is already a .pre-commit-config.yaml file.

::

    pre-commit install-hooks

::


    [INFO] Initializing environment for https://github.com/pre-commit/pre-commit-hooks.
    [INFO] Installing environment for https://github.com/pre-commit/pre-commit-hooks.
    [INFO] Once installed this environment will be reused.
    [INFO] This may take a few minutes...


pre-commit autoupdate
-----------------------

::

    pre-commit autoupdate

::

    Updating https://github.com/pre-commit/pre-commit-hooks...updating v1.4.0 -> v2.1.0.


::

    git commit . -m "Add .pre-commit-config.yaml"

::

    Trim Trailing Whitespace.................................................Passed
    Fix End of Files.........................................................Passed
    Check Yaml...............................................................Passed
    Check for added large files..............................................Passed
    [master b760551] Add .pre-commit-config.yaml
     2 files changed, 94 insertions(+), 5 deletions(-)
     create mode 100644 .pre-commit-config.yaml



pre-commit run --all-files
----------------------------

::

    pre-commit run --all-files

::

    pre-commit run --all-files
    Trim Trailing Whitespace.................................................Failed
    hookid: trailing-whitespace

    Files were modified by this hook. Additional output:

    Fixing README.md
    Fixing django_based/funkwhale/layout/api/.pylintrc

    Check Yaml...............................................................Passed
    Check for added large files..............................................Passed
    Makefile:26: recipe for target 'check_all' failed


Makefile
==========

We can use a Makefile.


.. literalinclude:: ../../Makefile
   :linenos:


Articles
==========

.. toctree::
   :maxdepth: 3

   articles/articles

.pre-commit-config.yaml examples
=================================

.. toctree::
   :maxdepth: 3

   examples/examples


List of hooks repos
======================

.. toctree::
   :maxdepth: 5

   repos_hooks/repos_hooks
