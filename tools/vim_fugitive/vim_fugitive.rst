
.. index::
   pair: Git client ; vim fugitive
   pair: Tool; vim fugitive


.. _vim_fugitive:

===================
Vim fugitive
===================

.. seealso::

   - https://github.com/VundleVim/Vundle.vim
   - https://github.com/tpope/vim-fugitive
   - https://www.vim.org/scripts/script.php?script_id=2975






Description
==============

A Git wrapper so awesome, it should be illegal.


Installation with Vundle
=========================

In your ~/.vimrc

::

    " The following are examples of different formats supported.
    " Keep Plugin commands between vundle#begin/end.
    " plugin on GitHub repo
    Plugin 'tpope/vim-fugitive'
