

.. _gitmoji_def:

===================================================================================
gitmoji-cli definition
===================================================================================

.. seealso::

   - https://github.com/carloscuesta/gitmoji-cli/

README.md
==========

.. seealso::

   - https://github.com/carloscuesta/gitmoji-cli/blob/master/README.md


.. include:: README.rst
