gitmoji-cli
===========

|Travis Build Status| |Code Climate| |Codecov| |David Dependencies| |npm
version| |npm downloads| |gitmoji badge|

.. figure:: https://cloud.githubusercontent.com/assets/7629661/20454643/11eb9e40-ae47-11e6-90db-a1ad8a87b495.gif
   :alt: gitmoji-cli

   gitmoji-cli

    A `gitmoji <https://github.com/carloscuesta/gitmoji>`__ interactive
    client for using gitmojis on commit messages.

About
-----

This project provides an easy solution for using
`**gitmoji** <https://github.com/carloscuesta/gitmoji>`__ from your
command line. Gitmoji-cli solves the hassle of searching through the
gitmoji list. Includes a bunch of options you can play with! :tada:

Install
-------

.. code:: bash

    $ npm i -g gitmoji-cli

Usage
-----

.. code:: bash

    $ gitmoji --help

::

    A gitmoji interactive client for using gitmojis on commit messages.

      Usage
        $ gitmoji
      Options
        --init, -i      Initialize gitmoji as a commit hook
        --remove, -r    Remove a previously initialized commit hook
        --config, -g    Setup gitmoji-cli preferences.
        --commit, -c    Interactively commit using the prompts
        --list, -l      List all the available gitmojis
        --search, -s    Search gitmojis
        --version, -v   Print gitmoji-cli installed version
        --update, -u    Sync emoji list with the repo

Commit
~~~~~~

You can use the commit functionality in two ways, directly or via a
commit-hook.

Client
^^^^^^

Start the interactive commit client, to auto generate your commit based
on your prompts.

.. code:: bash

    $ gitmoji -c

Hook
^^^^

Run the init option, add your changes and commit them, after that the
prompts will begin and your commit message will be built.

.. code:: bash

    $ gitmoji -i # this will create the .git/hooks/prepare-commit-msg
    $ git add .
    $ git commit

.. figure:: https://cloud.githubusercontent.com/assets/7629661/20454513/5db2750a-ae43-11e6-99d7-4757108fe640.png
   :alt: gitmoji commit

   gitmoji commit

Search
~~~~~~

Search using specific keywords to find the right gitmoji.

.. code:: bash

    $ gitmoji bug linter -s

.. figure:: https://cloud.githubusercontent.com/assets/7629661/20454469/1815550e-ae42-11e6-8c23-33ab7a3e48a3.png
   :alt: gitmoji list

   gitmoji list

List
~~~~

Pretty print all the available gitmojis.

.. code:: bash

    $ gitmoji -l

.. figure:: https://cloud.githubusercontent.com/assets/7629661/20454472/1c351e6c-ae42-11e6-8f3c-da73429d8eff.png
   :alt: gitmoji list

   gitmoji list

Update
~~~~~~

Update the gitmojis list, by default the first time you run gitmoji, the
cli creates a cache to allow using this tool without internet
connection.

.. code:: bash

    $ gitmoji -u

Config
~~~~~~

Run ``gitmoji -g`` to setup some gitmoji-cli preferences, such as the
auto ``git add .`` feature.

.. figure:: https://cloud.githubusercontent.com/assets/7629661/23577826/82e8745e-00c9-11e7-9d7e-623a0a51bff9.png
   :alt: gitmoji config

   gitmoji config

.. |Travis Build Status| image:: https://img.shields.io/travis/carloscuesta/gitmoji-cli.svg?style=flat-square
   :target: https://travis-ci.org/carloscuesta/gitmoji-cli
.. |Code Climate| image:: https://img.shields.io/codeclimate/maintainability/carloscuesta/gitmoji-cli.svg?style=flat-square
   :target: https://codeclimate.com/github/carloscuesta/gitmoji-cli
.. |Codecov| image:: https://img.shields.io/codecov/c/github/carloscuesta/gitmoji-cli.svg?style=flat-square
   :target: https://github.com/carloscuesta/gitmoji-cli
.. |David Dependencies| image:: https://img.shields.io/david/carloscuesta/gitmoji-cli.svg?style=flat-square
   :target: https://david-dm.org/carloscuesta/gitmoji-cli
.. |npm version| image:: https://img.shields.io/npm/v/gitmoji-cli.svg?style=flat-square
   :target: https://www.npmjs.com/package/gitmoji-cli
.. |npm downloads| image:: https://img.shields.io/npm/dt/gitmoji-cli.svg?style=flat-square
   :target: https://www.npmjs.com/package/gitmoji-cli
.. |gitmoji badge| image:: https://img.shields.io/badge/gitmoji-%20😜%20😍-FFDD67.svg?style=flat-square
   :target: https://github.com/carloscuesta/gitmoji
