
.. index::
   pair: Versions ; gitmoji

.. _gitmoji_versions:

===================================================================================
gitmoji-cli versions
===================================================================================

.. seealso::

   - https://github.com/carloscuesta/gitmoji-cli/releases

.. toctree::
   :maxdepth: 3

   1.9.2/1.9.2
