
.. index::
   pair: Git Tools ; gitmoji-cli
   ! gitmoji-cli
   ! gitmoji

.. _gitmoji_cli:

===================================================================================
gitmoji-cli (A gitmoji interactive command line tool for using emojis on commits)
===================================================================================

.. seealso::

   - https://github.com/carloscuesta/gitmoji-cli/
   - https://github.com/carloscuesta/gitmoji/


.. toctree::
   :maxdepth: 3

   definition/definition
   gitmoji_l/gitmoji_l
   versions/versions
