
.. index::
   pair: zsh; oh_my_zsh
   pair: Tool; oh_my_zsh
   pair: git; extras


.. _git_oh_my_zsh:

=====================
oh_my_zsh plugins
=====================

.. seealso::

   - :ref:`oh_my_zsh`
   - https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins

.. toctree::
   :maxdepth: 3

   git_extras/git_extras
   git_flow_avh/git_flow_avh
