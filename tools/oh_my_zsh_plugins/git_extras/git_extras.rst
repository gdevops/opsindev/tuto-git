
.. index::
   pair: oh_my_zsh; extras

.. _oh_myzsh_git_extras:

======================
oh_myzsh git-extras
======================

.. seealso::

  - :ref:`ref_git_extras`
  - https://github.com/agrimaldi
  - https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins#git-extras


Add git-extras in your plugins (file ~/zshrc)::

    plugins=(django docker git git-extras git-flow-avh pyenv tmux z)
