
.. index::
   pair: git ; show


.. _git_show:

===========================================
git show (Show various types of objects)
===========================================

- https://git-scm.com/docs/git-show


git show [commit]
==================

:Source: :ref:`froggit_tutorial`

::

    git show [commit]

affiche les métadonnées et le contenu du commit, ou du tag.


Show various types of objects
======================================

Shows one or more objects (blobs, trees, tags and commits).


How to see a specific commit in Git
========================================

- https://www.freecodecamp.org/news/git-cheat-sheet/

This command shows a specific commit.

Replace commit-id with the id of the commit that you find in the commit
log after the word commit.

::

    git show commit-id


For commits
=============

For commits it shows the log message and textual diff.

It also presents the merge commit in a special format as produced by
**git diff-tree --cc**.

git show commit example
-------------------------

::

    git show 3a807a6f597a97:django/contrib/admin/static/admin/js/SelectFilter2.js


::


    /*global SelectBox, gettext, interpolate, quickElement, SelectFilter*/
    /*
    SelectFilter2 - Turns a multiple-select box into a filter interface.

    Requires core.js and SelectBox.js.
    */
    (function() {
        'use strict';
        window.SelectFilter = {



For tags
=============

For tags, it shows the tag message and the referenced objects.

For trees
================

For trees, it shows the names (equivalent to git ls-tree with --name-only).


For plain blobs
===================

For plain blobs, it shows the plain contents.


The command takes options applicable to the git diff-tree command to control
how the changes the commit introduces are shown.

This manual page describes only the most frequently used options.


Examples
==========

git show HEAD~1:Pipfile.lock
-------------------------------

::

    git show HEAD~1:Pipfile.lock


Show Pipfile.lock one commit before.
