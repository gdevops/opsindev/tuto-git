.. index::
   pair: git ; reset

.. _git_reset:

============
git reset
============

.. seealso::

   - https://git-scm.com/docs/git-reset
   - https://github.blog/2015-06-08-how-to-undo-almost-anything-with-git/




git reset [fichier]
======================

:Source: :ref:`froggit_tutorial`

::

    git reset [fichier]

désindexe le fichier de l’index tout en conservant son contenu.

git reset [commit]
====================

:Source: :ref:`froggit_tutorial`

::

    git reset [commit]

annule tous les commits après commit, en conservant les modifications.

git reset --hard [commit]
==========================

:Source: :ref:`froggit_tutorial`

::

    git reset --hard [commit]


.. warning:: ⚠️ supprime tous les commits et les modifications effectuées après [commit]


Cancel our modifications (back to last commit)
================================================

To cancel our changes (equivalent of **svn revert**) to go back to our
last commit state::

    git reset HEAD
