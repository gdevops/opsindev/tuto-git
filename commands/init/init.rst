.. index::
   pair: git ; init

.. _git_init:

==========
git init
==========

.. seealso::

   - https://git-scm.com/docs/git-init



git init
=========

:Source: :ref:`froggit_tutorial`

::

    git init


transforme le répertoire courant en nouveau dépôt local.

git init [projet]
==================

:Source: :ref:`froggit_tutorial`

::

    git init [projet]


crée nouveau dépôt local dans le sous-répertoire [projet].

Init/clone repository
=======================

Init new project::

    cd /path/to/your/project
    git init
