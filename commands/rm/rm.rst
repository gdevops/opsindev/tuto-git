.. index::
   pair: git ; rm

.. _git_rm:

==========
git rm
==========

.. seealso::

   - https://git-scm.com/docs/git-rm
   - https://github.blog/2015-06-08-how-to-undo-almost-anything-with-git/




git rm [fichier]
==================

:Source: :ref:`froggit_tutorial`

::

    git rm [fichier]

supprime le fichier du répertoire de travail et met à jour l’index.

git rm --cached [fichier]
===========================

:Source: :ref:`froggit_tutorial`


::

    git rm --cached [fichier]

supprime le fichier du système de suivi de version, mais le préserve
localement.


Remove file from last commit
===============================

Let's say you committed a file by mistake. You can quickly remove that file
from the last commit by combining rm and commit --amend commands

::

    git rm --cached <file-to-remove>
    git commit --amend
