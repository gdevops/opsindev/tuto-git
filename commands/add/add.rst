
.. index::
   pair: git ; add

.. _git_add:

==========
git add
==========

- https://git-scm.com/docs/git-add


git add [fichier]
===================

:Source: :ref:`froggit_tutorial`


::

    git add [fichier]


**indexe** le fichier nommé fichier pour préparer le commit

Interactively Add Selected Parts of Files
==========================================

::

    git add -p

-p (or —patch) allows to interactively select parts of each tracked file to
commit.

This way each commit contains only related changes.

For more: git help add


How to add only certain files to the staging area in Git
========================================================

With the asterisk in the command below, you can add all files starting
with 'fil' in the staging area.

::

    git add fil*
