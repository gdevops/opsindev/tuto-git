.. index::
   pair: git ; mv

.. _git_mv:

==========
git mv
==========

.. seealso::

   - https://git-scm.com/docs/git-mv




git mv [fichier-nom] [fichier-nouveau-nom]
=============================================

:Source: :ref:`froggit_tutorial`


::

    git mv [fichier-nom] [fichier-nouveau-nom]

renomme le fichier et met à jour l’index.
