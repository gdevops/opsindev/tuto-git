
.. index::
   pair: git ; rebase


.. _git_rebase:

============
git rebase
============

.. seealso::

   - https://git-scm.com/docs/git-rebase
   - :ref:`git_cherry_pick`
   - :ref:`writing_clean_history`
   - https://git-scm.com/book/en/v2/Git-Branching-Rebasing
   - https://help.github.com/articles/about-git-rebase/
   - https://help.github.com/articles/using-git-rebase-on-the-command-line/
   - https://github.blog/2015-06-08-how-to-undo-almost-anything-with-git/


.. figure:: ../patching/patching.png
   :align: center

   Patching commands





git rebase --interactive HEAD~<number_of_local_commits>
==========================================================

::

    git rebase --interactive HEAD~<number_of_local_commits>


Examples
===========

- :ref:`git_rebase_3_1_1`
- :ref:`git_rebase_head_2`

.. toctree::
   :maxdepth: 3

   examples/examples


Rebase our current branch onto another branch
================================================

To rebase the 'feature' branch onto 'dev' branch :

::

    git checkout feature
    git rebase dev

Then, the next time you will push your code to the remote repository,
you will have to force the push in order to rewrite the git history.

::

    git push --force
