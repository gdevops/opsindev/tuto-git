
.. index::
   pair: rebase; example 1


.. _git_rebase_example_1:

======================
git rebase example 1
======================


.. seealso::

   - https://x.com/hybridcattt/status/1114596481359581185
   - https://x.com/hybridcattt



.. figure:: marina.png
   :align: center


I use rebase a lot, and find using remote branches so much easier
no need to get the local base branch up to date::

    git fetch
    git rebase origin/master


vs::

    git checkout master
    git pull
    git checkout mybranch
    git rebase master


Hope this helps someone make their workflow better.
