
.. index::
   pair: git ; bisect


.. _git_bisect:

===========
git bisect
===========

.. seealso::

   - https://git-scm.com/docs/git-bisect





Références
============

- GNU/Linux magazine France, N°215
  DevOps/git
  "La maintenance applicative avec git" p.22
