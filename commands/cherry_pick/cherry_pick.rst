
.. index::
   pair: git ; cherry-pick




.. _git_cherry_pick:

==================
git cherry-pick
==================

.. seealso::

   - https://git-scm.com/docs/git-cherry-pick
   - :ref:`git_rebase`





.. figure:: ../patching/patching.png
   :align: center

   Patching commands



Références
=============

- GNU/Linux magazine France, N°215
  DevOps/git
  "La maintenance applicative avec git" p.22
