.. index::
   pair: git ; checkout


.. _git_checkout:

==============================================================
git checkout (switch branches or restore working tree files)
==============================================================

.. seealso::

   - https://git-scm.com/docs/git-checkout
   - https://github.blog/2015-06-08-how-to-undo-almost-anything-with-git/
   - :ref:`git_branch`




Description
=============

Permet de changer de branche.

Exemple::

    git checkout blogpost


git checkout [nom-de-branche]
===============================

:Source: :ref:`froggit_tutorial`

::

    git checkout [nom-de-branche]


bascule sur la branche demandée, par défaut c’est master.

Create a new branch
=======================

To create a new branch and making it the current working branch

::

    git branch new-branch
    git checkout new branch

You can do the same with only one command::

    git checkout -b new-branch


Change the current working branch
=====================================

By default, we are on the 'master' branch.

To swith to another branch, we can do::

    git checkout branch-name



Interactively Revert Selected Parts of Files
=================================================

::

    git checkout -p

--patch can be also used to selectively discard parts of each tracked file.
I aliased this command as git discard

For more: git help checkout


Switch to Previous Branch
===========================

::

    git checkout -

This command allows you to quickly switch to the previously checked out branch.
On a general note - is an alias for the previous branch.

It can be used with other commands as well. I aliased checkout to co so, it
becomes just git co -


Revert All Local Changes
==========================

::

    git checkout .

If you are sure that all of your local changes can be discarded, you can use .
to do it at once.

It is, however, a good practice to always use checkout --patch.
