
.. index::
   pair: git ; push


.. _git_push:

=========
git push
=========

.. seealso::

   - https://git-scm.com/docs/git-push


.. figure:: ../sharing/sharing.png
   :align: center

   Sharing and updating projects




git push [alias] [branche]
==============================

:Source: :ref:`froggit_tutorial`

::

    git push [alias] [branche]

envoie tous les commits de la branche locale vers le serveur git.

Souvent l’alias est **origin**.


git push --tags
=================

::

    git push --tags

.. warning:: ⚠️envoie tous les tags locaux vers le serveur git distant.

git push [alias] [mon-tag]
============================

::

    git push [alias] [mon-tag]

envoie mon-tag de la branche locale vers le serveur git.

Souvent l’alias est **origin**.

Push our code/new branch to the remote repository
====================================================

To push our local 'mybranch' branch to the remote repository::

    git push origin mybranch


Push a new branch ont the remote server
=========================================

.. seealso::

   - :ref:`creation_feature_1`
   - :ref:`creation_feature_2`


::

    (tuto_git-QpDXWaZY) git checkout -b test_branch

::

    Basculement sur la nouvelle branche 'test_branch'

::

    (tuto_git-QpDXWaZY) $ git status

::

    Sur la branche test_branch
    rien à valider, la copie de travail est propre

::

    (tuto_git-QpDXWaZY) $ git push --set-upstream origin test_branch


::

    Total 0 (delta 0), reused 0 (delta 0)
    remote:
    remote: To create a merge request for test_branch, visit:
    remote:   https://gitlab.com/pvergain/tuto_git/merge_requests/new?merge_request%5Bsource_branch%5D=test_branch
    remote:
    To gitlab.com:pvergain/tuto_git.git
     * [new branch]      test_branch -> test_branch
    La branche 'test_branch' est paramétrée pour suivre la branche distante 'test_branch' depuis 'origin'.
