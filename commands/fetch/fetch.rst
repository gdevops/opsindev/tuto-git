.. index::
   pair: git ; fetch

.. _git_fetch:

===========
git fetch
===========

.. seealso::

   - https://git-scm.com/docs/git-fetch




git fetch [depot]
==================

:Source: :ref:`froggit_tutorial`

::

    git fetch [depot]

récupère tout l’historique du dépôt, sans modifier votre branche locale.

Update our local branches definition
======================================

In order to keep our local repository updated with the remote
repository, we can update our list of local branches :

::

    git fetch --all
