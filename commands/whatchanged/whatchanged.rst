
.. index::
   pair: git ; whatchanged


.. _git_whatchanged:

=================
git whatchanged
=================

.. seealso::

   - https://git-scm.com/docs/git-whatchanged





git whatchanged --since='1 day ago'
========================================
