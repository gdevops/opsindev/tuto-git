.. index::
   pair: git ; merge

.. _git_merge:

===========
git merge
===========

.. seealso::

   - https://git-scm.com/docs/git-merge
   - :ref:`merge_feature_devops_4`


.. figure:: ../branching/branching_and_merging.png
   :align: center

   Git branching and merging

git merge [nom-de-branche]
============================

:Source: :ref:`froggit_tutorial`

::

    git merge [nom-de-branche]


fusion dans la branche courante des commits de la branche.


git merge [depot]/[branche]
============================

:Source: :ref:`froggit_tutorial`

::

    git merge [depot]/[branche]

fusionne la branche du dépôt dans la branche locale ?


Merge a branch
=================

To merge some_fixes branch into feature branch::

    git checkout feature
    git merge some_fixes
