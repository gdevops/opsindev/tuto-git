
.. index::
   pair: git ; difftool


.. _git_difftool:

===============
git difftool
===============

.. seealso::

   - https://git-scm.com/docs/git-difftool






To configure Meld
====================

::

    $ git config --global diff.tool git-meld


git difftool $start_commit..$end_commit -- path/to/file
========================================================

To start viewing the diffs::

    $ git difftool $start_commit..$end_commit -- path/to/file

or::

    $ git difftool $start_commit..$end_commit
