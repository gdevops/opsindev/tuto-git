
.. index::
   pair: git ; pull


.. _git_pull:

=========
Git pull
=========

.. seealso::

   - https://git-scm.com/docs/git-pull
   - :ref:`git_pull_exemple`


.. figure:: ../sharing/sharing.png
   :align: center

   Sharing and updating projects




Description
============

Incorporates changes from a remote repository into the current branch.

In its default mode, git pull is shorthand for git fetch followed by
git merge FETCH_HEAD.

More precisely, git pull runs git fetch with the given parameters and
calls git merge to merge the retrieved branch heads into the current
branch.

With --rebase, it runs git rebase instead of git merge.

git pull
==========

:Source: :ref:`froggit_tutorial`

::

    git pull

récupère tout l’historique du dépôt et incorpore les modifications
en local.

Cela revient en fait à faire un fetch, puis un merge en une seule commande.

Keep our branch updated : get the remote repository changes
=============================================================

To push our local 'mybranch' branch to the remote repository::

    git pull


Rebase our code when pulling changes of the current branch
=============================================================

To get all the remote changes of the current branch, and keep my commits
at the end of the branch

::

    git pull --rebase

Then, the next time you will push your code to the remote repository,
you will have to force the push in order to rewrite the git history.

::

    git push --force
