.. index::
   pair: git ; commit
   pair: amend ; commit
   pair: co-authored ; Tips
   pair: git-commit ; Tips

.. _git_commit:

============
git commit
============

- https://github.blog/2015-06-08-how-to-undo-almost-anything-with-git/

git commit
============

::

    git commit

crée un commit avec tous les fichiers qui sont dans l’index.



git commit --allow-empty
============================

.. seealso::

   - https://x.com/meganesulli/status/1366466258393161729?s=20

TIL that you can do `git commit --allow-empty` to make a new commit with
no actual changes to your files.

No more random whitespace changes to trigger new builds!

.. figure:: git_allow_empty.png
   :align: center

   https://x.com/meganesulli/status/1366466258393161729?s=20


git commit -m "[description]" -m "description 2" ...
=======================================================

::

    git commit -m "[description]"


la même chose que **git commit**, mais en mettant le message du commit
directement dans la ligne de commande.


Modify The Most Recent Commit
=============================================

- https://css-irl.info/amending-your-past-commits-with-git/


::

    git commit --amend



Typos happen, but luckily in the case of commit messages, it is very
easy to fix them:

- git commit --amend                  # start $EDITOR to edit the message
- git commit --amend -m "New message" # set the new message directly

But that’s not all git-amend can do for you.
Did you forget to add a file ? Just add it and amend the previous commit !

::

    git add forgotten_file
    git commit --amend



--amend allows to append staged changes (e.g. to add a forgotten file) to the
previous commit.

Adding --no-edit on top of that will amend the last commit without changing its
commit message.
If there are no changes, --amend will allow you to reword the last commit message.


How to amend the most recent commit in Git
==============================================

- https://www.freecodecamp.org/news/git-cheat-sheet/

git commit --amend allows you to modify and add changes to the most
recent commit.


::

    git commit --amend

.. warning:: fixing up a local commit with amend is great and you can
   push it to a shared repository after you've fixed it.
   But you should avoid amending commits that have already been made public.

Commit some code
===================

**Code is not send to the remote repository. Our commit is not visible
by other people until we push the commit**

::

    git add new-file
    git commit -m "Here, we commit new-file to our local repo"

To commit all our changes :

::

    git add .
    git commit -m "Here, we commit all our modifications to our local repo"


.. _git_command_tips:

"Co-authored-by" Tips
========================

::

    git commit -m "Co-authored-by: XXX <xxx@aaa>"


::

    git commit -m "Update tips" -m "Co-authored-by: Yael"


.. figure:: co_authored_by.png
   :align: center

How to commit changes (and skip the staging area) in Git
===========================================================

You can add and commit tracked files with a single command by using
the -a and -m options.

::

    git commit -a -m"your commit message here"
