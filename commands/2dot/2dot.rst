
.. index::
   pair: git ; 2dot (..)
   pair: git diff; 2dot
   pair: git log; 2dot
   ! Double Dot Notation


.. _git_2dot:

===================================
".." command, Double Dot Notation
===================================




Double Dot Notation
=======================

.. seealso::

   - https://realpython.com/advanced-git-for-pythonistas/


The **double dot** method for specifying ranges looks like it sounds::

    git log b05022238cdf08..60f89368787f0e.

It’s tempting to think of this as saying “show me all commits after
b05022238cdf08 up to and including 60f89368787f0e” and, if b05022238cdf08
is a direct ancestor of 60f89368787f0e, that’s exactly what it does.


git diff .. <file>
========================

Example::

    git log <file>

    .. get list of commits

    git diff <commit_1>..<commit_2>  <file>


::

    [root@ansible ansible]# git diff 6393..661f README.md

::


	diff --git a/README.md b/README.md
	index 24a5c99..6734a24 100644
	--- a/README.md
	+++ b/README.md
	@@ -43,14 +43,18 @@ Exemples d'appel de la commande *ansible-playbook*
		 ansible-playbook -i inventory/hosts -l staging playbook.yml


	-Utilisation de Git
	-===================
	+Utilisation de commandes git
	+==============================

	 - git status
	 - git add .
	 - git commit . -m "commentaire"
	 - git push
	 - git pull
	+- git tag <tag_name>
	+- git push origin --tags
	+- git log <file>
	+- git diff <commit_1>..<commit_2> <file>
