
.. index::
   pair: git ; ls-files

.. _git_ls_files:

===============
git ls-files
===============

.. seealso::

   - https://git-scm.com/docs/git-ls-files




git ls-files
==============

:Source: :ref:`froggit_tutorial`

::

    git ls-files --other --ignored --exclude-standard


liste tous les fichiers exclus du suivi de version dans ce projet.
