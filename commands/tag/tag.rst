
.. index::
   pair: git ; tag

.. _git_tag:

=========
git tag
=========

.. seealso::

   - https://git-scm.com/docs/git-tag




.. figure:: ../branching/branching_and_merging.png
   :align: center

   Git branching and merging


git tag [ mon-tag ] <[commit]>
=================================

.. seealso::

   - :ref:`git_log_follow`
   - https://semver.org/lang/fr/

:Source: :ref:`froggit_tutorial`

::

    git tag [mon-tag] <[commit]>


**étiquette le commit**, ou par défaut le dernier commit, avec le tag.

Ce tag est souvent un numéro de version et peut servir à faire des
releases.
Pour standardiser tes numéros de version, regarde du côté du `versionnage sémantique`_.


.. _`versionnage sémantique`:  https://semver.org/lang/fr/

Manage tags
==============

To list all available tags::

    git tag


Example 1
----------

To create a new tag::

    git tag -a v1.0 -m "Version 1.0"

Send the tag to the remote repository::

    git push origin v1.0

Example 2
----------

To create a new tag::

    git tag -a 0.7.7 -m "Version 0.7.7 (2019-09-26)"

Send the tag to the remote repository::

    git push origin 0.7.7
