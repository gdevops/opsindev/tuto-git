.. index::
   pair: git ; branch
   pair: help ; branch
   ! branch

.. _git_branch:

==========
git branch
==========

.. seealso::

   - https://git-scm.com/docs/git-branch
   - :ref:`git_checkout`





git branch
============

:Source: :ref:`froggit_tutorial`

::

    git branch

affiche toutes les branches du dépôt local.


git branch [nom-de-branche]
=============================

:Source: :ref:`froggit_tutorial`

::

    git branch [nom-de-branche]


crée une nouvelle branche.

git branch -m [nom-de-branche] [nouveau-nom]
===============================================

:Source: :ref:`froggit_tutorial`


::

    git branch -m [nom-de-branche] [nouveau-nom]


change le nom de la branche.

Create a new branch
=======================

To create a new branch and making it the current working branch

::

    git branch new-branch
    git checkout new branch

You can do the same with only one command::

    git checkout -b new-branch


Delete a branch
=================

::

    git branch -D <branch>


Find Branches
===============

::

    git branch --contains <commit>


This command will show all branches that contain a particular commit.


Rename Branches Locally
=========================

::

    git branch -m old-name new-name

If you want to rename the currently checked out branch, you can shorten this
command to the following form::

    git branch -m new-name

For more: git help branch


Rename Branches Remotely
===========================

In order to rename a branch remotely, once you renamed your branch locally,
you need to first remove that branch remotely and then push the renamed
branch again::

    git push origin old-name
    git push origin new-name


git help branch
=================

.. literalinclude:: help_branch.txt
   :linenos:
