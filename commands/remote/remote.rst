
.. index::
   pair: git ; remote


============
git remote
============

.. seealso::

   - https://git-scm.com/docs/git-remote






git remote -v
===============


::

    git remote -v

::

    old-origin      git@xxxx:pvergain/logtransactions_colombie.git (fetch)
    old-origin      git@xxxx:pvergain/logtransactions_colombie.git (push)
    origin  http://git@xxxx/Informatique/logtransactions_colombie.git (fetch)
    origin  http://git@xxxx/Informatique/logtransactions_colombie.git (push)
