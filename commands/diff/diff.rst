
.. index::
   pair: git ; diff


.. _git_diff:

==========
git diff
==========

- https://git-scm.com/docs/git-diff


.. figure:: ../basic_snapshot/basic_snapshot.png
   :align: center

   Basic snapshot


git diff [br1] [br2]
=======================

:Source: :ref:`froggit_tutorial`

::

    git diff [br1] [br2]

affiche les différences entre deux branches.

git diff --word-diff
======================

When editing prose, as opposed to code, it can often be much more useful
to see changed words rather than whole changed lines; this is
particularly helpful when writing markdown, like I am right now.

Thankfully, we can show only changes words by using the --word-diff
flag.

::

    git diff --word-diff

Running a diff without the --word-diff flag shows quite a large
difference::

    -My friend Tom recently gave an excellent talk
    +My good friend Tom gave an excellent talk

but rerunning the diff with --word-diff enabled gives us a much more
digestible and helpful overview::

    My {+good+} friend Tom [-recently-] gave an excellent talk

git diff $start_commit..$end_commit -- path/to/file
=====================================================

:Source: https://about.gitlab.com/blog/2020/04/07/15-git-tips-improve-workflow/


If you want to compare the same file between different commits, you run
the following:

    $ git diff $start_commit..$end_commit -- path/to/file


git diff $start_commit..$end_commit
====================================

:Source: https://about.gitlab.com/blog/2020/04/07/15-git-tips-improve-workflow/


If you want to compare the changes between two commits::


    $ git diff $start_commit..$end_commit


Open All Files with Conflicts at Once
============================================

Rebasing may lead to conflicts, the following command will open all files which
need your help to resolve these conflicts::

    git diff --name-only --diff-filter=U | uniq  | xargs $EDITOR


Source: https://dev.to/zaiste/15-git-commands-you-may-not-know-4a8



git diff commit
===================

::

    $ git diff 3a807a6f597a97 django/contrib/admin/static/admin/js/SelectFilter2.js

::

    diff --git a/django/contrib/admin/static/admin/js/SelectFilter2.js b/django/contrib/admin/static/admin/js/SelectFilter2.js
    index 8ea9bc3e47..b4f5c06803 100644
    --- a/django/contrib/admin/static/admin/js/SelectFilter2.js
    +++ b/django/contrib/admin/static/admin/js/SelectFilter2.js
    @@ -95,7 +95,7 @@ Requires core.js and SelectBox.js.
                     )
                 );

    -            var to_box = quickElement('select', selector_chosen, '', 'id', field_id + '_to', 'multiple', 'multiple', 'size', from_box.size, 'name', from_box.name);
    +            var to_box = quickElement('select', selector_chosen, '', 'id', field_id + '_to', 'multiple', '', 'size', from_box.size, 'name', from_box.name);
                 to_box.className = 'filtered';
                 var clear_all = quickElement('a', selector_chosen, gettext('Remove all'), 'title', interpolate(gettext('Click to remove all chosen %s at once.'), [field_name]), 'href', '#', 'id', field_id + '_remove_all_link');
                 clear_all.className = 'selector-clearall';


Show changes
=============

::

    git diff --staged

This command shows all staged changes (those added to the index) in contrast
to just git diff which only shows changes in the working directory (without
those in the index).

For more: git help diff



How to see changes made before committing them using "diff" in Git
=======================================================================

- https://www.freecodecamp.org/news/git-cheat-sheet/

You can pass a file as a parameter to only see changes on a specific file.
git diff shows only unstaged changes by default.

We can call diff with the **--staged flag to see any staged changes**::

    git diff
    git diff all_checks.py
    git diff --staged
