.. index::
   pair: git ; commands

.. _git_commands:

===================
Git commands
===================

- https://git-scm.com/docs
- https://firstaidgit.io
- https://services.github.com/on-demand/downloads/fr/github-git-cheat-sheet/
- https://gitlab.com/help/topics/git/index.md
- https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet
- https://www.freecodecamp.org/news/git-cheat-sheet/


.. toctree::
   :maxdepth: 3

   2dot/2dot
   add/add
   bisect/bisect
   branch/branch
   cherry_pick/cherry_pick
   clone/clone
   commit/commit
   config/config
   diff/diff
   difftool/difftool
   checkout/checkout
   fetch/fetch
   filter_repo/filter_repo
   gc/gc
   help/help
   init/init
   log/log
   ls_files/ls_files
   merge/merge
   mv/mv
   pull/pull
   push/push
   rebase/rebase
   reflog/reflog
   remote/remote
   reset/reset
   restore/restore
   revert/revert
   rm/rm
   show/show
   status/status
   stash/stash
   switch/switch
   tag/tag
   whatchanged/whatchanged
   worktree/worktree
