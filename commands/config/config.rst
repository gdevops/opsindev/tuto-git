
.. index::
   pair: git ; config


.. _git_config:

============
git config
============

   - https://www.codementor.io/citizen428/git-tutorial-10-common-git-problems-and-how-to-fix-them-aajv0katd

.. _rerere:

rerere (Avoid repeated merge conflicts)
==========================================

- https://git-scm.com/docs/git-config


::

    git config --global rerere.enabled true


Tweak your Git config for multiple user IDs
==================================================


- https://www.ecosia.org/search?q=Tweak+your+Git+config+for+multiple+user+IDs&addon=firefox&addonversion=4.0.4
- https://opensource.com/article/20/10/git-config


Description
--------------

Git's git config command (hereafter referred to as "config") enables you
to set repository or global options for Git.

It has many options, and one of them, includeIf, is handy when you have
dual roles using Git, for example, working full time as a developer and
**contributing to open source projects in your spare time**.

Most people in this situation don't want to use a common config for
both roles or, at the very least, would rather keep certain portions
of their config distinct, especially if they use the same computer
in both roles.


How to check your Git configuration
======================================

The command below returns a list of information about your git configuration
including user name and email:

::

    git config -l
