
.. index::
   pair: git ; gc


.. _git_gc:

==========
git gc
==========

.. seealso::

   - https://git-scm.com/docs/git-gc





Optimize the repository locally
==================================

::

    git gc --prune=now --aggressive


For more: git help gc
