
.. index::
   pair: git ; revert


.. _git_revert:

============
git revert
============

.. seealso::

   - https://git-scm.com/docs/git-revert



Revert the state of our branch (undo changes and commits)
============================================================

**This command is not the equivalent of svn revert. Be carefull.**

With the revert command, we can undo all our current not commited
changes, **and all our not pushed commits**.

::

    git revert
