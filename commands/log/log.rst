
.. index::
   pair: git ; log
   pair: git ; shortlog


.. _git_log:

==========
git log
==========


- https://git-scm.com/docs/git-log
- https://www.atlassian.com/git/tutorials/git-log
- https://mirrors.edge.kernel.org/pub/software/scm/git/docs/git-log.html


.. figure:: ../branching/branching_and_merging.png
   :align: center

   Git branching and merging

.gitconfig
============

::

    ($ cat ~/.gitconfig


    [user]
        name = Noam
        email = noamsw@pm.me
    [core]
        editor = vim
    [push]
        default = simple
    [alias]
        dlog = log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
        ci = commit
        co = chekout
        st = status


How to see your commit history in Git
=========================================

This command shows the commit history for the current repository::

    git log



How to see log stats in Git
===============================

- https://www.freecodecamp.org/news/git-cheat-sheet/

This command will cause the Git log to show some statistics about the
changes in each commit, including line(s) changed and file names.


::

    git log --stat



How to show the commit log as a graph in Git
==================================================

- https://www.freecodecamp.org/news/git-cheat-sheet/

We can use --graph to get the commit log to show as a graph.
Also, --oneline will limit commit messages to a single line::


    git log --graph --oneline


.. _git_log_follow:

git log --follow [fichier]
============================

:Source: :ref:`froggit_tutorial`

::

    git log --follow [fichier]


affiche la liste des **commits** du fichier.


git log --author=[auteur]
===========================

:Source: :ref:`froggit_tutorial`


::

    git log --author=[auteur]


affiche tous les commits de l’auteur.


git dlog (alias)
==================

- :ref:`git_log`


::

    git dlog


::

    *   8ad7236 - (HEAD -> 2.2.0, tag: 2.2.1, origin/2.2.0) Merge branch 'hotfix-2.2.1' into 2.2.0 (il y a 53 minutes) <Noam>
    |\
    | * e02812a - Cheery-pick 2.2.0 Fix 3.1.0 (il y a 58 minutes) <Noam>
    |/
    * 6d0b2c5 - Add version 2.2.0 (il y a 2 heures) <Noam>
    * 2fe3273 - (origin/2.1.0, 2.1.0) Add 2.1.0 (il y a 3 heures) <Noam>
    * 0c78ff0 - (origin/2.0.0, 2.0.0) 2.0.0 OK (il y a 20 heures) <Noam>
    * 845b87e - version 2 (il y a 20 heures) <Noam>
    *   228bb80 - Merge branch '1.0.0' (il y a 20 heures) <Noam>
    |\
    | * ab53dcc - (origin/1.0.0, 1.0.0) Add Changelog (il y a 20 heures) <Noam>
    |/
    *   bd3c428 - Merge branch 'feature-2' into 'master' (il y a 21 heures) <Vergain>
    |\
    | * 832f65c - Update feature 2 step3 (il y a 22 heures) <Noam>
    | * 7b75abf - Update feature 2 step2 (il y a 22 heures) <Noam>
    | * 3ba0cb3 - Update feature 2 step1 (il y a 22 heures) <Noam>
    | * b723d6c - Begin implementing feature 2 (il y a 23 heures) <Noam>
    |/
    *   005c357 - Merge branch 'feature-1' (il y a 25 heures) <Noam>
    |\
    | * 3803afe - Add new comments on feature 1 (il y a 25 heures) <Noam>
    | * 8e58300 - Add comments on feature 1 (il y a 26 heures) <Noam>
    | * e5359ee - Implement feature1 (il y a 26 heures) <Noam>
    |/



git dlog -p
===============


Affiche les lignes modifiées avec l'option -p.

::

    git dlog -p


::

    *   8ad7236 - (HEAD -> 2.2.0, tag: 2.2.1, origin/2.2.0) Merge branch 'hotfix-2.2.1' into 2.2.0 (il y a 55 minutes) <Noam>
    |\
    | * e02812a - Cheery-pick 2.2.0 Fix 3.1.0 (il y a 61 minutes) <Noam>
    |/  |
    |   diff --git a/Changelog b/Changelog
    |   index 0e4f0cf..80d53c0 100644
    |   --- a/Changelog
    |   +++ b/Changelog
    |   @@ -2,6 +2,29 @@ Changelog
    |    ===========
    |
    |
    |   +<<<<<<< HEAD
    |   +=======
    |   +
    |   +3.1.1 (2018-03-15)
    |   +=====================
    |   +
    |   +- Fix 3.1.0
    |   +- rebase with master after missing creation of 2.1.0 and 2.2.0.
    |   +
    |   +3.1.0 (2018-03-15)




git shortlog
==============

::

    git shortlog


::

    Noam (46):
          add README
          Add .gitgnore + .gitlab-ci.yml
          forgot to add .gitignore
          add requirements.txt
          End workflow generality
          Add parallele chapter
          add 'bien nommer ses branches'
          Add chapitre conventions
          Add releases + bug fixes
          add links
          update links conventions
          Version 1.0.0
          forget instruction
          add versions + begin gitlab flow
          gitlab introduction
          Added github introduction
          Added git flow and its problems
          Added github flow as a simpler alternative
          Added Production branch with GitLab flow
          update Production branch with GitLab flow
          Add Release branches with GitLab flow
          Add Merge/pull requests with GitLab flow
          Add Issue tracking with GitLab flow
          Update Issue tracking with GitLab flow
          Added Squashing commits with rebase



git shortlog -sn --all --no-merges
=====================================

If you’re interested in seeing who has committed how much to your
project, shortlog can give us the answer.

::

    tuto_git $ git shortlog -sn --all --no-merges

::

    55  Noam
     1  Patrick Vergain


git log --graph --oneline --decorate
========================================

::

    git log --graph --oneline --decorate


::

    * 555a5e9 (HEAD -> master, origin/master) Update why using git
    * aa987e6 Update migration from svn to git
    * 682e5f7 Add migrate from svn to git
    * 871732a Add Help github
    * bb0cd16 Update git rebase
    * e734e8b Add git scm book
    * c5fbaf5 Add links between github and gitlab
    * 34fca56 complete the github flow
    * 3cc39e6 Update fix_bugs
    * 3f69971 Update parallele chapter
    * a18bf85 more links for the creation of remote branches ...
    * b89293d more links for the creation of remote branches
    * 7f39861 renommage des features
    * fce31cc Add push command
    * 2899015 (origin/test_branch, test_branch) Add commands chapter
    * 122e853 How to go the master branch
    * 6f89800 Add howto chapter
    * 06e1718 (1.1.0) Version 1.1.0 gitlab flow
    * ef928ca (tag: 1.1.0) Add Working with feature branches
    * b40e604 Add commit often with the right message
    * 356bd37 Update commits with rebase



git log --pretty=format:"%cn committed %h on %cd"
====================================================

::

    git log --pretty=format:"%cn committed %h on %cd"

::

    Noam committed 555a5e9 on Wed Mar 14 11:35:26 2018 +0100
    Noam committed aa987e6 on Wed Mar 14 11:13:27 2018 +0100
    Noam committed 682e5f7 on Wed Mar 14 11:06:53 2018 +0100
    Noam committed 871732a on Wed Mar 14 11:00:44 2018 +0100
    Noam committed bb0cd16 on Wed Mar 14 10:54:31 2018 +0100
    Noam committed e734e8b on Wed Mar 14 10:37:10 2018 +0100
    Noam committed c5fbaf5 on Wed Mar 14 09:42:50 2018 +0100
    Noam committed 34fca56 on Wed Mar 14 09:35:36 2018 +0100
    Noam committed 3cc39e6 on Wed Mar 14 09:10:54 2018 +0100
    Noam committed 3f69971 on Wed Mar 14 08:40:45 2018 +0100


git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
===================================================================================================================================

::

    git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit


::

    * 555a5e9 - (HEAD -> master, origin/master) Update why using git (il y a 89 minutes) <Noam>
    * aa987e6 - Update migration from svn to git (il y a 2 heures) <Noam>
    * 682e5f7 - Add migrate from svn to git (il y a 2 heures) <Noam>
    * 871732a - Add Help github (il y a 2 heures) <Noam>
    * bb0cd16 - Update git rebase (il y a 2 heures) <Noam>
    * e734e8b - Add git scm book (il y a 2 heures) <Noam>
    * c5fbaf5 - Add links between github and gitlab (il y a 3 heures) <Noam>
    * 34fca56 - complete the github flow (il y a 3 heures) <Noam>
    * 3cc39e6 - Update fix_bugs (il y a 4 heures) <Noam>
    * 3f69971 - Update parallele chapter (il y a 4 heures) <Noam>
    * a18bf85 - more links for the creation of remote branches ... (il y a 20 heures) <Noam>
    * b89293d - more links for the creation of remote branches (il y a 20 heures) <Noam>
    * 7f39861 - renommage des features (il y a 20 heures) <Noam>
    * fce31cc - Add push command (il y a 21 heures) <Noam>
    * 2899015 - (origin/test_branch, test_branch) Add commands chapter (il y a 21 heures) <Noam>
    * 122e853 - How to go the master branch (il y a 21 heures) <Noam>
    * 6f89800 - Add howto chapter (il y a 21 heures) <Noam>
    * 06e1718 - (1.1.0) Version 1.1.0 gitlab flow (il y a 22 heures) <Noam>
    * ef928ca - (tag: 1.1.0) Add Working with feature branches (il y a 22 heures) <Noam>
    * b40e604 - Add commit often with the right message (il y a 23 heures) <Noam>
    * 356bd37 - Update commits with rebase (il y a 23 heures) <Noam>
    * 54dd9e2 - Added Squashing commits with rebase (il y a 23 heures) <Noam>
    * 651f547 - Update Issue tracking with GitLab flow (il y a 24 heures) <Noam>
    * c665167 - Add Issue tracking with GitLab flow (il y a 27 heures) <Noam>


git log --graph --all --decorate --stat --date=iso
=====================================================

his will give me --graph-based logs for --all branches showing commit
--stats (additions, deletions). --decorate will give me information
about which branch a commit was made on where applicable, and I
also get a much stricter date format.


::


    git log --graph --all --decorate --stat --date=iso


::

    * commit afaf232e7a150a33b6b7a5a3fe685b5cbfb52f76 (HEAD -> master, origin/master, origin/HEAD)
    | Author: Patrick Vergain <pvergain@gmail.com>
    | Date:   2018-03-14 21:18:25 +0100
    |
    |     Add fullstackpython
    |
    |  configuration/configuration.rst               |  3 ++-
    |  tutorials/fullstackpython/fullstackpython.rst | 17 +++++++++++++++++
    |  tutorials/tutorials.rst                       |  1 +
    |  3 files changed, 20 insertions(+), 1 deletion(-)
    |
    * commit aba6ab1a0edd3495bc59eaeb9fae92315dae2e6a
    | Author: Noam <noamsw@pm.me>
    | Date:   2018-03-14 16:53:47 +0100
    |
    |     Update sogilis
    |
    |  flows/sogilis/sogilis.rst | 341 ++++++++++++++++++++++++++++++++++++------------------------------------
    |  1 file changed, 171 insertions(+), 170 deletions(-)
    |
