.. index::
   pair: git ; reflog

.. _git_reflog:

============
git reflog
============

.. seealso::

   - https://git-scm.com/docs/git-reflog
   - https://github.blog/2015-06-08-how-to-undo-almost-anything-with-git/
