
.. index::
   pair: git ; filter-repo


.. _git_filter_repo:

===============================
git filter-repo (git 2.24.0+)
===============================

.. seealso::

   - https://htmlpreview.github.io/?https://github.com/newren/git-filter-repo/blob/docs/html/git-filter-repo.html#EXAMPLES
