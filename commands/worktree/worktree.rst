
.. index::
   pair: git ; worktree


.. _git_worktree:

=============
git worktree
=============


.. seealso::

   - https://git-scm.com/docs/git-worktree






Références
===========

VictorStinner
---------------

- https://x.com/VictorStinner/status/1379941702513922048?s=20


It helps a lot for Python incremental build and compare 2 versions::

    $ git worktree list

::

    ../python/master  df5dc1c7a5 [master]
    ../python/2.7     8d21aa21f2 [heads/2.7]
    (...)
    ../python/3.7     2f01c562be [3.7]
    ../python/3.8     4554ab4357 [3.8]
    ../python/3.9     3b1cf20297 [3.9]r/status/1379941702513922048?s=20


GNU/Linux magazine France, N°215
-----------------------------------

- GNU/Linux magazine France, N°215
  DevOps/git
  "La maintenance applicative avec git" p.24
