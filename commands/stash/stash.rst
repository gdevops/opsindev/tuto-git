.. index::
   pair: git ; stash

.. _git_stash:

==========
git stash
==========

.. seealso::

   - https://git-scm.com/docs/git-stash




git stash
===========

:Source: :ref:`froggit_tutorial`

::

    git stash

enregistre de manière temporaire tous les fichiers qui ont été modifiés.
On appelle cela **remiser son travail**.

git stash list
=================

:Source: :ref:`froggit_tutorial`

::

    git stash list

affiche la liste de toutes les remises.


git stash pop
==============

:Source: :ref:`froggit_tutorial`

::

    git stash pop

applique une remise sur le répertoire courant et la supprime immédiatement.

git stash drop
=================

:Source: :ref:`froggit_tutorial`

::

    git stash drop

supprime la remise la plus récente.

Record our changes and restore them
======================================

To save our current changes and revert to the last commit::

    git stash


To reapply the saved changes::

    git stash apply

Or, if you want reapply the changes and delete the record::

    git stash pop
