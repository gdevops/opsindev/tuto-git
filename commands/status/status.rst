.. index::
   pair: git ; status

.. _git_status:

==========
git status
==========

.. seealso::

   - https://git-scm.com/docs/git-status





git status
=============

:Source: :ref:`froggit_tutorial`

::

    git status

affiche tous les fichiers qui peuvent être commités (ajoutés, supprimés,
renommés, modifiés).

Check the state of our local repository
=========================================

::

    git status
