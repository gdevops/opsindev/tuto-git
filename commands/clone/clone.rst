
.. index::
   pair: git ; clone

.. _git_clone:

===========
git clone
===========

.. seealso::

   - https://git-scm.com/docs/git-clone





git clone [url]
================

:Source: :ref:`froggit_tutorial`

::

    git clone [url]

fait une copie locale d’un projet depuis un dépôt distant

Clone existing project
=========================

::

    git clone <repo_URL>
