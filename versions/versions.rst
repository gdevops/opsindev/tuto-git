.. index::
   pair: Git ; Versions

.. _git_versions:

===================
Versions
===================

- https://github.com/git/git/tags
- https://github.com/git/git/tree/v2.48.0/Documentation/RelNotes


.. toctree::
   :maxdepth: 3

   2.48.0/2.48.0
   2.44.0/2.44.0
   2.43.0/2.43.0
   2.38.0/2.38.0
   2.33.0/2.33.0
   2.31.0/2.31.0
   2.30.0/2.30.0
   2.28.0/2.28.0
   2.26.0/2.26.0
   2.25.0/2.25.0
   2.24.0/2.24.0
   2.23.0/2.23.0
   2.21.0/2.21.0
   2.19.0/2.19.0
   2.13.0/2.13.0
