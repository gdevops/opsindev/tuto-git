
.. index::
   pair: Why ; Git
   pair: Agile  ; Business
   pair: Why not ; SVN


.. _why_git:

==========================
Why
==========================

.. figure:: are-you-too-busy-to-improve2.png
   :align: center

.. seealso::

   - :ref:`svn_to_git`





The DevOps tool
================


.. seealso::

   - https://docs.gitlab.com/ee/topics/autodevops/
   - https://appdevelopermagazine.com/5674/2017/11/15/gitlab-tells-us-about-auto-devops/
   - https://about.gitlab.com/2017/10/04/devops-strategy/


In the next five years, our world will continue to become more
software-defined and as developers begin to adopt DevOps practices,
they will need tools that accelerate workflow and **simplify the
development process**.

While DevOps has a great deal of potential, organizations tend to
continue to have technology and role complexity with both developers and
operators with some degree of incremental DevOps technology at the
intersection between the two.

We believe this divide will begin to close and developers and operators
will both find themselves with more seamless options that allow them to
move faster from idea to production.

.. figure:: devops-strategy-venn-diagram.png
   :align: center

   https://about.gitlab.com/2017/10/04/devops-strategy/


GitLab: 2018 is the year for open source and DevOps
-----------------------------------------------------

.. seealso::

   - https://sdtimes.com/devops/gitlabs-2018-devops-open-source-survey/

The demand for DevOps continues to grow, **even though there are still
challenges created by outdated tools and company resistance to change**.

According to the report, only 23 percent identify DevOps as their
development methodology. However, IT management has named DevOps as one
of the top three areas of investment in 2018, indicating that the number
of DevOps adopters is sure to grow this year.

GitLab found that most developers are in agreement that DevOps workflows
save time during the development process.
Teams that have adopted DevOps have confirmed that there is an increase
in productivity, allowing them to spend at least 50 percent of their
time on new work.


Build, Test, Store and Distribute Docker Containers with GitLab
----------------------------------------------------------------

.. seealso::

   - https://www.willhallonline.co.uk/blog/build-test-store-and-distribute-docker-containers-gitlab

The use of Docker containers has exploded (at least within Web Development)
and therefore we have a whole bunch of new tools that can be used to
manage the DevOps process.
However, there are a massive range of tools that can be used to achieve
all of these. Be it Git hosting (GitHub, Bitbucket); Continuous
Integration and Delivery and Build Tools (Jenkins, CircleCI, Drone);
Docker Registry (Docker EE, Quay.io), or a range of other add-on tools,
there sometimes seem as many new, great products in the DevOps area as
there are Javascript frameworks(!).

However, I feel that GitLab offers a single solution that is able to
compete with other established tools and potentially enable you to
have a full professional delivery platform for free (within some
restrictions on numbers).



It's critical for organizations to use the latest development tools
=====================================================================


.. figure:: square-wheels-one-lego-idea-for-improvement-quote.jpg
   :align: center

.. _gitlab_arguments:

Gitlab arguments
==================

.. figure:: arguments.png
   :align: center

   81% say it's critical for organizations to use the latest development tools

   https://get.gitlab.com/global-developer-survey/


2017
------

.. seealso::

   - https://about.gitlab.com/2017/05/17/learning-curve-is-the-biggest-challenge-developers-face-with-git/


Over the last decade, distributed version control systems, like Git,
have gained popularity and are regarded as the most important
development tools by developers.

Although the learning curve can pose a challenge, developers told us
that **Git enhances their ability to work together** and ship faster,
suggesting that managers have a real incentive to help their teams
over the initial hill imposed by the transition to Git.

With the full history of the repository stored on each developer’s
machine, using Git makes commits, merges and other commands much faster,
even enabling developers to work offline.

Upgrading your source code management solution to a distributed version
control system is the first step toward building a flexible working
environment that can support modern development teams, but moving away
from :ref:`legacy systems and tools can be a daunting prospect <svn_to_git>`.


Git for developers
====================

Feature Branch Workflow
---------------------------

.. seealso::

   - :ref:`intro_branches`
   - https://www.atlassian.com/agile/backlogs
   - https://www.atlassian.com/software/jira

One of the biggest advantages of Git is its branching capabilities.

Unlike centralized version control systems, :ref:`Git branches <intro_branches>`
are **cheap and easy to merge**.

This facilitates the **feature branch workflow** popular with many Git users.

Feature branches provide an isolated environment for every change to
your codebase.

When a developer wants to start working on something—no matter how big
or small—they create a new branch. This ensures that the master branch
always contains production-quality code.

Using feature branches is not only more reliable than directly editing
production code, but it also provides organizational benefits.

They let you represent development work at the same granularity as the
your agile backlog. For example, you might implement a policy where
each Jira ticket is addressed in its own feature branch.

Distributed Development
=========================

In SVN, each developer gets a **working copy that points back to a single
central repository**.

**Git, however, is a distributed version control system**.

Instead of a working copy, each developer gets their own local repository,
complete with a full history of commits.

.. figure:: svn_versus_git.svg
   :align: center

   svn versus git


**Having a full local history makes Git fast**, since it means you don’t
need a network connection to create commits, inspect previous versions
of a file, or perform diffs between commits.

Distributed development also makes it easier to scale your engineering
team.

If someone breaks the production branch in SVN, other developers can’t
check in their changes until it’s fixed.

**With Git, this kind of blocking doesn’t exist**.

Everybody can continue going about their business in their own local
repositories.

And, similar to feature branches, distributed development creates a
more reliable environment. Even if a developer obliterates their own
repository, they can simply clone someone else’s and start anew.
