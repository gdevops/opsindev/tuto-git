.. index::
   pair: git ; master to main
   ! branch main

.. _git_master_to_main:

=================
master to main
=================

.. figure:: main_default_branch.png
   :align: center



Setting the default branch to main
====================================

::

    git config --global init.defaultBranch main

Opérations
=============

- :ref:`git_2_31_0`
- https://about.gitlab.com/releases/2021/01/22/gitlab-13-8-released/

::

    git branch -m master main
    git push -u origin main
    modifier gitlab-ci.yaml
    update the default repository
    delete the master branch


Update the default repository
===================================

.. figure:: default_repository.png
   :align: center

   https://gitlab.com/gdevops/tuto_git/-/settings/repository


delete the master branch
===========================

.. figure:: delete_the_master_branch.png
   :align: center


.. _git_2_31_0_master_to_main:

Git 2.31.0 (2021-03-15)
==========================

.. seealso::

   - https://about.gitlab.com/releases/2021/01/22/gitlab-13-8-released/
   - https://lore.kernel.org/git/xmqqa6vf437i.fsf@gitster.c.googlers.com/T/#t
   - :ref:`project:gitlab_default_main`


**Git version 2.31.0 (scheduled for release March 15th, 2021) will change
the default branch name in Git from master to main.**

In coordination with the Git project and the broader community, GitLab
will be changing the default branch name for new projects on both our
Saas (GitLab.com) and self-managed offerings **starting with GitLab 14.0**.


.. figure:: info_gitlab.png
   :align: center

   https://about.gitlab.com/releases/2021/01/22/gitlab-13-8-released/



Python 3.11 (2021-03-03) master => main
===========================================

.. seealso::

   - https://mail.python.org/archives/list/python-dev@python.org/thread/QWW7KGBW5UH2N5FOZOFXQBQPYELWQM3O/


Introduction
------------

Hi,

I am writing on behalf of the Python Release Management team. The Steering
Council has requested the RM team to schedule
and perform the necessary changes to rename the default branch from master
to main.

The changes
-------------

What follows is the technical description of the changes and the timeline.
In order to keep this thread focused on this particular
aspect, if you wish to discuss anything related to the change itself,
please, open a new email thread or reuse an existing one.

- The change will be executed when beta 1 is released, as the beta 1
  release requires some branching engineering already to create the 3.10
  branch and point 3.11 to the new one as well as changing CI, buildbots...etc *This is scheduled for Monday, 2021-05-03.*
- The CI will be adapted to work with the new "main" branch.
- The buildbots will be adapted to work with the new "main" branch.
- Branch protection rules will be adapted.
- The different bots will be adapted by the respective bot maintainer teams.
- All the URLs that point to master in the README and other places will
  be adapted to point to main instead (notice this is not strictly necessary
  because GitHub redirects automatically).

Notice that the renaming will automatically:

- Re-target any open pull requests
- Update any draft releases based on the branch
- Move any branch protection rules that explicitly reference the old name
- Show a notice to repository contributors, maintainers, and admins on the repository homepage with instructions to update local copies of the repository
- Show a notice to contributors who git push to the old branch
- Redirect web requests for the old branch name to the new branch name
- Return a "Moved Permanently" response in API requests for the old branch name

Check this https://github.com/github/renaming for more information.

What you need to do ?
--------------------------

You just need to update your local clone after the branch name changes.
From the local clone of the repository on a computer,
run the following commands to update the name of the default branch.

::

    $ git branch -m master main
    $ git fetch origin
    $ git branch -u origin/main main

Apart from that, you should update any local script or command that uses
the name "master" to use the name "main".

Regards from windy London,
Pablo Galindo Salgado


.. figure:: python_3_10/github_master_to_main.png
   :align: center


.. figure:: python_3_10/master_to_main.png
   :align: center




gitlab  The new Git default branch name Why we're joining the Git community and updating "master" to "main"
================================================================================================================

- https://about.gitlab.com/blog/2021/03/10/new-git-default-branch-name/

GitLab has already introduced changes that allow users to change the
default branch name both at the instance-level (for self-managed users)
and at the group-level (for both SaaS and self-managed users).

To further align with the community, we will make the following changes
in the upcoming releases:

Default branch name changes for GitLab.com and self-managed users: When
creating a new project in GitLab, the default branch name will change
from master to main. We are planning to implement this change in two phases:

Phase 1:
---------

We plan to update GitLab's underlying Git version to 2.31.0 as
part of GitLab’s 13.11 release, shipping on April 22, 2021.

The branch name change will be shipped under a feature flag.
Projects created from GitLab will continue to the current default branch
name of master.

Phase 2:
-----------

We will remove the feature flag as part for our next major release 14.0,
shipping on May 22, 2021.

Any project created in GitLab will use the default branch name of main.

Action required: When upgrading a self-managed instance to GitLab 14.0,
new projects created via the GitLab GUI will use main as the default
branch name.

If your project is running CI/CD jobs configured in another project,
hard-coded references to master will need to be updated to use either
main or to use a default branch name CI/CD variable, such as CI_DEFAULT_BRANCH.

You can continue to use master by configuring a custom default branch name.

git How to alias “master” as 'main'
=======================================

- :ref:`git_alias_master_as_main_2022_10_27`
