


.. index::
   pair: zulip ; Git


.. _zulip_tuto:

====================================
Zulip git tutorial
====================================

.. seealso::

   - https://zulip.readthedocs.io/en/latest/git/overview.html


We use a **rebase-oriented workflow**.

We do not use merge commits.
This means you should use git fetch followed by git rebase rather than
git pull (or you can use git pull --rebase).

Also, to prevent pull requests from becoming out of date with the main
line of development, you should rebase your feature branch prior to
submitting a pull request, and as needed thereafter.

If you’re unfamiliar with how to rebase a pull request, read this
excellent guide.

We use this strategy in order to avoid the extra commits that appear
when another branch is merged, that clutter the commit history
(it’s popular with other large projects such as Django).

This makes Zulip’s commit history more readable, but a side effect is
that many pull requests we merge will be reported by GitHub’s UI as
closed instead of merged, since GitHub has poor support for
rebase-oriented workflows.
