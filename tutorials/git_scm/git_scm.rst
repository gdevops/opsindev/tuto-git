


.. index::
   pair: Scm ; Git


.. _git_scm_book:

====================================
Git-scm book + git-scm doc
====================================

.. seealso::

   -  https://git-scm.com/docs





Traduction française
========================

.. seealso::

   - https://git-scm.com/book/fr/v2


English translation
====================

.. seealso::

   - https://git-scm.com/book/en/v2
