


.. index::
   pair: realpython ; Advanced Git Tips for Python Developers


.. _advanced_git_for_pythonistas:

=========================================
Advanced Git Tips for Python Developers
=========================================

.. seealso::

   - https://realpython.com/advanced-git-for-pythonistas/
