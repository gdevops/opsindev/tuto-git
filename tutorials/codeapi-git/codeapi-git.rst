.. index::
   pair: Tutorial ; Anton Zhiyanov (codeapi-git)

.. _zhiyanov_tuto_2024_03_11:

===============================================
**codeapi-git** by Anton Zhiyanov
===============================================

- https://codapi.org/git/
- https://c.im/@antonz/112077536188283003


I keep forgetting git syntax, so I made this interactive cookbook with all
the weird incantations for different use cases.

It's also a safe place for your wildest git fantasies, should you have any.
