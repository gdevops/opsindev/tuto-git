
.. index::
   pair: Git ; Froggit
   pair: DevOps ; compagnons-devops.fr

.. _froggit_tutorial:

===============================================
Froggit tutorial par **Christophe Chaudier**
===============================================

.. seealso::

   - https://www.compagnons-devops.fr/
   - https://forum.compagnons-devops.fr
   - https://x.com/art_devops
   - https://lydra.fr/blog/
   - https://lydra.fr/




Source: Christophe Chaudier
==============================

Christophe Chaudier
Co-fondateur de Lydra de Froggit et des Compagnons du DevOps

.. figure:: cchaudier.jpg
   :align: center

Commandes abordées
-------------------

- :ref:`git_add`
- :ref:`git_branch`
- :ref:`git_checkout`
- :ref:`git_clone`
- :ref:`git_commit`
- :ref:`git_diff`
- :ref:`git_fetch`
- :ref:`git_init`
- :ref:`git_log`
- :ref:`git_ls_files`
- :ref:`git_merge`
- :ref:`git_mv`
- :ref:`git_pull`
- :ref:`git_push`
- :ref:`git_reset`
- :ref:`git_rm`
- :ref:`git_show`
- :ref:`git_stash`
- :ref:`git_status`

Glossaire
==========

.. seealso::

   - :ref:`glossaire_git_francais`

- :term:`branche`
- :term:`dépôt`
- :term:`fusion`
- :term:`tirer`
- :term:`rapporter`

:download:`Télécharger Froggit git antiseche froggit_git_antiseche.pdf <froggit_git_antiseche.pdf>`
