


.. _advanced_git_flow_memo:

====================================
Advanced git flow memo
====================================

.. seealso::

   - https://gitlab.com/JBodineau/git-memo






Revert the state of our branch (undo changes and commits)
==========================================================

**This command is not the equivalent of svn revert. Be carefull.**

With the revert command, we can undo all our current not commited
changes, **and all our not pushed commits**.

::

    git revert

Rebase our current branch onto another branch
=============================================

To rebase the 'feature' branch onto 'dev' branch::

    git checkout feature
    git rebase dev

Then, the next time you will push your code to the remote repository,
you will have to force the push in order to rewrite the git history.

::

    git push --force

Rebase our code when pulling changes of the current branch
==========================================================

To get all the remote changes of the current branch, and keep my commits
at the end of the branch

::

    git pull --rebase

Then, the next time you will push your code to the remote repository,
you will have to force the push in order to rewrite the git history.

::

    git push --force

Use git flow
============

Initialize Git flow in a project

::

    git flow init

To start a new development branch.

::

    git flow feature start dev_branch_name

is equivalent to

::

    git checkout -b dev_branch_name dev

To finish an existing development branch.

::

    git flow feature finish dev_branch_name

is equivalent to

::

    git checkout dev
    git merge dev_branch_name --no-ff
    git branch -d dev_branch_name

You can do the same with releases and hotfixes.

::

    git flow release start release_branch_name
    git flow release finish release_branch_name
    git flow hotfix start release_branch_name
    git flow hotfix finish release_branch_name

For release and hotfixes, the equivalent are

::

    git checkout -b new_branch_name master

and

::

    git checkout dev
    git merge new_branch_name --no-ff
    git checkout master
    git merge new_branch_name --no-ff
    git tag v1.0
    git branch -d dev_branch_name
