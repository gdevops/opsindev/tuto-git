

.. _basic_git_flow_memo:

====================================
Basic git flow memo
====================================

.. seealso::

   - https://gitlab.com/JBodineau/git-memo





Init/clone repository
=====================

Init new project::

    cd /path/to/your/project
    git init

Clone existing project::

    git clone <repo_URL>

Check the state of our local repository
=======================================

::

    git status

Change the current working branch
=================================

By default, we are on the 'master' branch.

To swith to another branch, we can do::

    git checkout branch-name

Update our local branches definition
====================================

In order to keep our local repository updated with the remote
repository, we can update our list of local branches :

::

    git fetch --all

Create a new branch
===================

To create a new branch and making it the current working branch

::

    git branch new-branch
    git checkout new branch

You can do the same with only one command :

::

    git checkout -b new-branch

Merge a branch
==============

To merge 'some\_fixes' branch into 'feature' branch::

    git checkout feature
    git merge some_fixes

Commit some code
================

**Code is not send to the remote repository. Our commit is not visible
by other people until we push the commit**

::

    git add new-file
    git commit -m "Here, we commit new-file to our local repo"

To commit all our changes :

::

    git add .
    git commit -m "Here, we commit all our modifications to our local repo"

Modify the previous commit
==========================

We can modify the previous commit (change the content, or the commit
message) with :

::

    git commit --amend

Or if we only want to change the content without modify the commit
message :

::

    git commit --amend --no-edit

Push our code/new branch to the remote repository
=================================================

To push our local 'mybranch' branch to the remote repository::

    git push origin mybranch


Keep our branch updated : get the remote repository changes
===========================================================

To push our local 'mybranch' branch to the remote repository :

::

    git pull

Cancel our modifications (back to last commit)
==============================================

To cancel our changes (equivalent of **svn revert**) to go back to our
last commit state :

::

    git reset HEAD

Record our changes and restore them
===================================

To save our current changes and revert to the last commit

::

    git stash

To reapply the saved changes

::

    git stash apply

Or, if you want reapply the changes and delete the record

::

    git stash pop

Manage tags
===========

To list all available tags

::

    git tag

To create a new tag

::

    git tag -a v1.0 -m "Version 1.0"

Send the tag to the remote repository

::

    git push origin v1.0
