#!/usr/bin/env sh
git checkout dev
git reset --hard 31bcbb1646a1d3c565f84eb7bb02fb782d872582
git push --force
git branch -D feature_A
git branch -D feature_B
git push origin --delete feature_A
git push origin --delete feature_B

rm -rf temp
