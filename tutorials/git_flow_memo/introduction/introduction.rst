
.. index::
   pair: Git; Good practices


.. _intro_git_flow_memo:

====================================
git flow memo introduction
====================================

.. seealso::

   - https://gitlab.com/JBodineau/git-memo




Good practices
===============

-  Never push directly on the master branch
-  Never push directly on the current development branch
-  Always create your own branch when working on a feature
-  Rebase your branch before merge it
-  Always use merge request (or pull request for github) to ask to merge
   your branch on the current development (or feature) branch
-  Be sure to understand what you do

Branches model and git architecture
====================================

.. seealso::

   - :ref:`git_flow`

`See here <https://nvie.com/posts/a-successful-git-branching-model/>`__
