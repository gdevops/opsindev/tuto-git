
.. index::
   pair: Git ;  installation


.. _git_installation:

================
Installation
================





CentOS 7
==========

.. seealso::

   - https://stackoverflow.com/questions/21820715/how-to-install-latest-version-of-git-on-centos-6-x-7-x

::

	$ sudo yum install https://centos7.iuscommunity.org/ius-release.rpm
	$ sudo yum erase git
	$ sudo yum install epel-release
	$ sudo yum install git2u


git clients
=============

.. toctree::
   :maxdepth: 3


   git_clients/git_clients
