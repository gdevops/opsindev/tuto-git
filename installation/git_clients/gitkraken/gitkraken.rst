
.. index::
   pair: Git ; Gitkraken


.. _gitkraken:

=====================
Gitkraken
=====================

.. seealso::

   - https://www.gitkraken.com/
   - https://support.gitkraken.com/how-to-install/
   - https://git.wiki.kernel.org/index.php/InterfacesFrontendsAndTools#GitKraken


.. figure:: mother-of-krakens.png
   :align: center


.. figure:: gitkraken-logo-light-hz.svg
   :align: center





Definition
===========

.. seealso::

   - https://git.wiki.kernel.org/index.php/InterfacesFrontendsAndTools#GitKraken

is a popular Git GUI client for Windows, Mac and Linux.

It's free for non-commercial use.

It's a great tool for Git beginners and advanced users to increase
efficiency through the intuitive interface, seamless integrations
and a faster, more fluid workflow.


Debian based distributions
============================

GitKraken has a simple package available for Debian based distributions.

::

    wget https://release.gitkraken.com/linux/gitkraken-amd64.deb
    dpkg -i gitkraken-amd64.deb


::

    wget https://release.gitkraken.com/linux/gitkraken-amd64.deb

::

    --2019-03-28 08:02:01--  https://release.gitkraken.com/linux/gitkraken-amd64.deb
    Résolution de release.gitkraken.com (release.gitkraken.com)… 18.235.209.28
    Connexion à release.gitkraken.com (release.gitkraken.com)|18.235.209.28|:443… connecté.
    requête HTTP transmise, en attente de la réponse… 302 Moved Temporarily
    Emplacement : https://release.axocdn.com/linux/gitkraken-amd64.deb [suivant]
    --2019-03-28 08:02:01--  https://release.axocdn.com/linux/gitkraken-amd64.deb
    Résolution de release.axocdn.com (release.axocdn.com)… 104.25.203.22, 104.25.202.22, 2606:4700:20::6819:ca16, ...
    Connexion à release.axocdn.com (release.axocdn.com)|104.25.203.22|:443… connecté.
    requête HTTP transmise, en attente de la réponse… 200 OK
    Taille : 64746744 (62M) [application/octet-stream]
    Enregistre : «gitkraken-amd64.deb»



.. figure:: menu_gitkraken_linuxmint.png
   :align: center
