
.. index::
   pair: Git ; GUI Clients


.. _git_clients:

=====================
Git GUI clients
=====================

.. seealso::

   - https://git-scm.com/downloads/guis/
   - https://git.wiki.kernel.org/index.php/InterfacesFrontendsAndTools


.. toctree::
   :maxdepth: 3


   gitkraken/gitkraken
