
.. _git_def:

================
Definition
================


.. seealso::

   - https://git-scm.com/
   - https://en.wikipedia.org/wiki/Git





Definition
============

.. seealso::

   - https://en.wikipedia.org/wiki/Git


Git (/ɡɪt/[7]) is a distributed version-control system for tracking
changes in source code during software development.

It is designed for coordinating work among programmers, but it can be
used to track changes in any set of files. Its goals include speed,
data integrity, and support for distributed, non-linear workflows.

Git was created by Linus Torvalds in 2005 for development of the Linux
kernel, with other kernel developers contributing to its initial
development.

Its current maintainer since 2005 is Junio Hamano.

As with most other distributed version-control systems, and unlike
most client–server systems, every Git directory on every computer is a
full-fledged repository with complete history and full version-tracking
abilities, independent of network access or a central server.


Naming
=========

Torvalds quipped about the name git (which means unpleasant person in
British English slang): "I'm an egotistical bastard, and I name all my
projects after myself. First 'Linux', now 'git'

The man page describes Git as "the stupid content tracker"

The readme file of the source code elaborates further

The name "git" was given by Linus Torvalds when he wrote the very first
version. He described the tool as "the stupid content tracker" and the
name as (depending on your way):

random three-letter combination that is pronounceable, and not actually
used by any common UNIX command. The fact that it is a mispronunciation
of "get" may or may not be relevant.

stupid. contemptible and despicable. simple. Take your pick from the
dictionary of slang.

"global information tracker": you're in a good mood, and it actually
works for you. Angels sing, and a light suddenly fills the room.

"goddamn idiotic truckload of sh*t": when it breaks


gitops
=========

.. toctree::
   :maxdepth: 3

   gitops/gitops


Glossaires
==============

.. toctree::
   :maxdepth: 3

   glossaires/glossaires
