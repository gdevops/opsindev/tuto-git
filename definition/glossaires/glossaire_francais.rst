.. index::
   ! Glossaire

.. _glossaire_git_francais:

==========================================
Glossaire git francophone
==========================================

:Source: :ref:`froggit_tutorial`


.. glossary::

   branch
   branche
   branches
       🇬🇧 branch

       Une branche c’est une version du dépôt où tu peux travailler sur
       une fonctionnalité particulière sans impacter le code courant.

       Il y a toujours une branche par défaut, c’est souvent master, et
       tu peux avoir autant de branches en cours que tu veux.

   commit
       C’est une enveloppe qui contient une petite portion de codes
       modifiés d’un ou plusieurs fichiers.
       Fais des commits les plus petits possible.

       Tu peux comparer les commits entre eux et voir les évolutions du
       code de ton projet.

       On utilise le verbe **commiter**.

   remote
   dépôt
       🇬🇧 repository ou remote.

       C’est le lieu où sont stockés tous les fichiers sources de ton projet.

       Le dépôt peut être un répertoire local sur ta machine ou un répertoire
       distant sur un serveur git.

       Le dépôt distant peut être accessible en https ou en ssh.

       Utilise plutôt une connexion ssh (avec clé SSH personnelle) qui est
       plus sécurisée et cela t’évite aussi de créer un fichier avec tes
       informations d’authentifications.

   PR
   MR
   fusion
       🇬🇧 merge

       Cette action applique les changements d’une branche sur une autre.

       Sur un serveur git, tu peux faire une demande de fusion qui peut
       être revue par tes collègues avant la fusion réelle.

       Sur Github, c’est une Pull Request (PR)
       Sur GitLab une Merge Request (MR)

   HEAD
       C’est un pointeur vers le dernier commit de la branche en cours.


   push
   pousser
       🇬🇧 push

       Cette action partage les commits du dépôt local avec le dépôt
       distant associé au projet en cours.

   fetch
   rapporter
       🇬🇧 fetch

       Cette action récupère les commits du dépôt distant en local sans
       les appliquer.

   tag
       C’est un pointeur vers un commit particulier.
       On s’en sert pour faire des releases.

   pull
   tirer
       🇬🇧 pull

       Cette action télécharge les commits manquant du dépôt distant sur
       notre dépôt local et les applique.

   Index
   staging area
   Zone de transit
       🇬🇧 staging area ou Index

       Cette zone mémoire contient les fichiers que tu mets de côté pour
       préparer le commit.

       On dit que les fichiers sont indexés.
