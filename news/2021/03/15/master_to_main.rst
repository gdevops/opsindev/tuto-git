.. index::
   pair: git ; master to main

.. _news_git_2_31_0:

==========================
Git 2.31.0 (2021-03-15)
==========================

.. seealso::

   - https://lore.kernel.org/git/xmqqa6vf437i.fsf@gitster.c.googlers.com/T/#t
   - :ref:`project:gitlab_default_main`


**Git version 2.31.0 (scheduled for release March 15th, 2021) will change
the default branch name in Git from master to main.**

In coordination with the Git project and the broader community, GitLab
will be changing the default branch name for new projects on both our
Saas (GitLab.com) and self-managed offerings **starting with GitLab 14.0**.
