.. index::
   pair: gitlab ; git-replay

.. _git_replay_2024_03_24:

===================================================================
2024-03-24 GitLab's contributions to Git 2.44.0 (git-replay)
===================================================================

- https://about.gitlab.com/blog/2024/02/26/gitlabs-contributions-to-git-2-44-0/


Find out the topics that GitLab’s Git team – as well as the wider community – contributed
to the latest Git release, including fast scripted rebases via git-replay.


Enter git-replay, which is a new command that does essentially the same thing
as git-rebase but in-memory, thus not requiring a worktree anymore.

This is an important building block to allow us to develop faster rebasing
of merge requests in the future.

You may ask: Why a new command instead of updating git-rebase?

The problem here was that git-rebase is essentially a user-focused command
(also called a "porcelain" command in Git). Thus it performs several actions
that are not required by a script at all, like, for example, executing hooks
or checking out files into the worktree.

The new git-replay command is a script-focused command (also called a "plumbing"
command in Git) and has a different set of advantages and drawbacks.

Furthermore, besides doing rebases, we plan to use it to do cherry-picks and
reverts in the future, too.

This topic was a joint effort by Elijah Newren and Christian Couder.
