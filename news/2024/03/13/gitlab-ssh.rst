.. index::
   pair: gitlab ; ssh

.. _gitlab_ssh:

==================================
2024-03-13 **gitlab SSH**
==================================

- https://adamtheautomator.com/gitlab-ssh/

Introduction
===============

Repeated username and password prompts are annoying and time-wasting.

If you use password authentication with your Gitlab account, each action
requires you to send over your credentials, whether as part of the command
or through an interactive prompt.



Adding the Gitlab SSH Key to Your Profile
==============================================

Remember that the private key stays on your computer, while the public key
should be on the Gitlab server.

So after generating the SSH Keys, your next step is to upload the public key
to your Gitlab account.


Using Your Gitlab SSH Key
===============================

So far, you’ve generated the SSH key and uploaded it to your Gitlab account.
What’s left is to test whether your SSH key works by using it to connect and
authenticate to Gitlab.


On the terminal, connect to your Gitlab account by running the command below.

::

    pvergain@load-testing:~$ ssh -T git@gitlab.srv.int.id3.eu


ssh -T git@gitlab.srv.int.id3.eu
----------------------------------

::

    ssh -T git@gitlab.srv.int.id3.eu

::

    Welcome to GitLab, @pvergain!
