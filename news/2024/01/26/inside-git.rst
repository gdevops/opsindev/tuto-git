.. index::
   pair: Julia Evans ; inside git

.. _evans_2024_01_26:

=============================================
2024-01-26  **Inside git** by Julia Evans
=============================================

- https://jvns.ca/blog/2024/01/26/inside-git/


Hello !

I posted a comic on Mastodon this week about what’s in the .git directory
and someone requested a text version, so here it is.

I added some extra notes too.

First, here’s the image.

It’s a ~15 word explanation of each part of your .git directory

.. figure:: images/inside-git.png
