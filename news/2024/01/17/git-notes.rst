.. index::
   pair: git ; notes

.. _git_notes:

===================================================================
2024-01-17 **Git Notes: git's coolest, most unloved­ feature**
===================================================================

- https://tylercipriani.com/blog/2022/11/19/git-notes-gits-coolest-most-unloved-feature/

Introduction
===============

**Git notes are almost a secret**.

They’re buried by their own distressing usability.

But git notes are **continually rediscovered by engineers** trying to stash
metadata inside git.

🧐 What are git notes ?
============================

A common use of git notes is **tacking metadata onto commits**.

Once a commit cements itself in git’s history—that’s it.
It’s impossible to amend a commit message buried deep in a repo’s log1.

But git notes enable you to amend new information about old commits in
a special namespace. And they’re capable of so much more.

Notes stow metadata about anything tracked by git—any object: commits,
blobs, and trees. All without futzing with the object itself.

You add notes to the latest commit in a repo like this::

    git notes add -m 'Acked-by: <tyler@tylercipriani.com>'

And then it shows up in git log::

    commit 1ef8b30ab7fc218ccc85c9a6411b1d2dd2925a16
    Author: Tyler Cipriani <thcipriani@gmail.com>
    Date:   Thu Nov 17 16:51:43 2022 -0700

        Initial commit

        Notes:
            Acked-by: <tyler@tylercipriani.com>


😭 No one uses git notes
====================================

Git notes are a pain to use.

And GitHub opted to stop displaying commit notes in 2014 without much explanation.

For commits, you can make viewing and adding notes easier using fancy
options in your gitconfig. But for storing notes about blobs or trees ?
Forget it. You’d need to be comfortable rooting around in git’s plumbing first.


. _config_git_notes:

Config git notes
-------------------

Noteably by automagically fetching notes and displaying them in git log via:::

    $ git config --add remote.origin.fetch '+refs/notes/*:refs/notes/*'
    $ git config notes.displayRef 'refs/notes/*'

So, for now: git notes are relegated to obscurity.

Forever hamstrung by an obscure and clunky interface and limited adoption—I
often forget they’re there.

🗽 Forge independence
=========================

Git is a distributed code review system. But much of the value of git repos
ends up locked into forges, like GitHub.

Git notes are a path toward an alternative.

Git distributes the history of a piece of code.

Git notes could make it possible to distribute the history of an entire project.
