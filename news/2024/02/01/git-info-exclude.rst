.. index::
   ! git-info-exclude

.. _git_info_exclude:

================================================================================================================
2024-02-01 Pour n’ignorer des fichiers que sur son propre poste, il faut les placer dans .git/info/exclude
================================================================================================================

- https://fr.wikibooks.org/wiki/Git/Exclure_des_fichiers_du_d%C3%A9p%C3%B4t

Description
=============


Pour n’ignorer des fichiers que sur son propre poste, il faut les placer
dans .git/info/exclude.

Ainsi, ils ne seront jamais proposés pendant les commits.


Recherche
============

Pour déterminer les fichiers ignorés d'un dossier ou pourquoi un fichier
est ignoré, utiliser "check-ignore". Exemple :

git check-ignore core/scripts/imagecopy.py -v
