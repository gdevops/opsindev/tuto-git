
.. index::
   pair: git ; 2017

.. _git_2017:

===================
Git news 2017
===================


[Traduction] Dans les entrailles de Git
==========================================

- https://maryrosecook.com/blog/post/git-from-the-inside-out
- https://x.com/maryrosecook
- https://nicolas.loeuillet.org/billets/2017/02/07/traduction-dans-les-entrailles-de-git/
- https://x.com/nicosomb/status/1519579338244513793?s=20&t=pQDA2vMVc03NwL0_jnEJ_w
- https://x.com/nicosomb

Ce billet est une traduction de l’excellent billet de Mary Rose Cook,
Git from the inside out. On y apprend vraiment plein de choses sur le
fonctionnement de Git.

Il se peut qu’il reste quelques coquilles, n’hésitez pas à me les signaler.

Je tiens à remercier Pierre Ozoux, goofy, Agnès H, Stéphane Hulard, Jums,
Julien aka Sphinx et xi de m’avoir aidé à traduire ce très très long billet.

Cet article explique comment fonctionne Git. Il part du principe que vous
comprenez suffisamment Git pour l’utiliser en tant que système de gestion
de versions pour vos projets.

Cet article se concentre sur la structure de graphe sur laquelle
s’appuie Git et sur la manière dont les propriétés de ce graphe dictent
le comportement de Git.

Revenir aux bases vous permet de visualiser les choses telles qu’elles
sont réellement, et vous évite d’échafauder des hypothèses à partir du
fonctionnement de l’API.
Ce modèle, plus fidèle à la réalité, vous permettra de mieux comprendre
ce que Git a fait, ce qu’il fait et ce qu’il fera.

Cet article est structuré comme une série de commandes Git exécutées
sur un projet.
Par moments, il y a des remarques à propos de la structure de données en
graphe sur laquelle Git est construit. Ces remarques illustrent une
propriété du graphe et le comportement provoquée par celle-ci.


Summary
-----------

Git is built on a graph. Almost every Git command manipulates this graph.
To understand Git deeply, focus on the properties of this graph, not
workflows or commands.

To learn more about Git, investigate the .git directory. It’s not scary.
Look inside. Change the content of files and see what happens.
Create a commit by hand. Try and see how badly you can mess up a repo.
Then repair it.

1. In this case, the hash is longer than the original content.
   But, all pieces of content longer than the number of characters in a
   hash will be expressed more concisely than the original. ↩

2. There is a chance that two different pieces of content will hash to
   the same value. But this chance is low. ↩

3. git prune deletes all objects that cannot be reached from a ref.
   If the user runs this command, they may lose content. ↩

4. git stash stores all the differences between the working copy and the
   HEAD commit in a safe place. They can be retrieved later. ↩

5. The rebase command can be used to add, edit and delete commits in the
   history. ↩
