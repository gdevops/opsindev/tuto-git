
.. _git_alias_master_as_main_2022_10_27:

=========================================================================================================
2022-10-27 **git How to alias “master” as 'main'** by Adam Johnson (https://x.com/AdamChainz)
=========================================================================================================

- https://x.com/AdamChainz/status/1585558912702480385?s=20&t=QKX8kEFsTDNZNRTHCbZtUg
- https://adamj.eu/tech/2022/10/27/git-how-to-alias-master-as-main/


Master to main
=================

- :ref:`git_master_to_main`

Introduction
========================

The Git community is gradually moving the default branch name from “master”
to “main”, because the term “master” is hurtful to some people as it
refers to slavery.

:ref:`Git 2.28 (2020-07-27) <git_2_28_0>` introduced the **init.defaultBranch option**,
which controlls the default branch name for repos created with git init:

    $ git config --global init.defaultBranch main

Git will move the default value for this setting from “master” to “main”
in due course.

Major Git hosting services already use “main” as the default for repos
created through their interfaces: BitBucket, GitHub, and GitLab.


Pro Aliasing
===============

It can be annoying to work on legacy repos that still have a “master” branch.
Your muscle memory or command aliases might use “main”, causing your
commands to fail:

::

    $ git switch main

::

    fatal: invalid reference: main

Sad times.

Luckily, Git offers a solution: local aliases, called “symbolic refs”,
which you can configure with git symbolic-ref.

You can make main an alias for master like so::

    $ git symbolic-ref refs/heads/main refs/heads/master

And origin/main an alias for origin/master with::

    $ git symbolic-ref refs/remotes/origin/main refs/remotes/origin/master

You can then switch to the aliased main branch::

    $ git switch main

Switched to branch 'main'

…and use main anywhere you’d previously need master::

    $ git rebase -i main
    ...
    $ git log origin/main..example
    ...
    $ # etc.

Nice one!


Git will show both main and master in some views, like git log::

    $ git log --oneline

::

    0e78b70 (HEAD -> master, main) Make the damn thing
    275a9fd (origin/master, origin/main) Initial commit

Good to know.
