
.. _git_autocorrect_2022_10_26:

=================================================================================================
2022-10-26 **git how to enable autocorrect** by Adam Johnson (https://x.com/AdamChainz)
=================================================================================================

- https://x.com/AdamChainz/status/1585377113020198912?s=20&t=QKX8kEFsTDNZNRTHCbZtUg
- https://adamj.eu/tech/2022/10/26/git-how-to-enable-autocorrect/


.. _git_autocorrect:

To enable autocorrect
========================

To enable autocorrect, set the help.autocorrect::

    $ git config --global help.autocorrect immediate

This will add to your ~/.gitconfig file::

    [help]
        autocorrect = immediate
