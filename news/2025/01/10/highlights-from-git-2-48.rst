
.. _git_news_2024:

===========================================
2025-01-10  **Highlights from Git 2.48**
===========================================

- https://github.blog/open-source/git/highlights-from-git-2-48/


Bringing --remerge-diff to range-diff
=========================================

- https://github.blog/open-source/git/highlights-from-git-2-19/#user-content-compare-histories-with-git-range-diff



Introducing Meson into Git
================================

- https://github.blog/open-source/git/highlights-from-git-2-48/#introducing-meson-into-git

In this release, the Git project has support for a new build system, Meson, 
as an alternative to building with GNU Make. While support for Meson is not 
yet as robust as building with Make, Meson does offer a handful of advantages 
over Make. 

Meson is easier to use than Make, making the project more accessible to 
newcomers or contributors who don’t have significant experience working 
with Make. 

Meson also has extensive IDE support, and supports out-of-tree and cross-platform 
builds, which are both significant assets to the Git project.

Git will retain support for Make and CMake in addition to Meson for the 
foreseeable future, and retain support for Autoconf for a little longer. 

But if you’re curious to experiment with Git’s newest build system, you can 
run::

    meson setup build && ninja -C build 
    
on a fresh copy of Git 2.48 or newer.
