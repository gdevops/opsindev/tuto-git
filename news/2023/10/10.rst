.. index::
   pair: Migration; Github to Sourcehut
   pair: jetporch; Github to Sourcehut

.. _git_news_2023_10:

==================================================================
2023-10 (le projet jetPorch a été abandonné fin décembre 2023)
==================================================================



.. _jetporch_sourcehut_2023_10_07:

2023-10-07 **jetporch: SourceHut Move Complete, Examples and Inventory Repos Merged**
=======================================================================================

- https://www.jetporch.com/
- https://jetporch.substack.com/p/sourcehut-move-complete-examples
- https://www.jetporch.com/community/sourcehut

Our source code move to SourceHut and permanently away from GitHub is now complete.

GitHub remains online as a read-only emergency backup and mirror.

The GitHub issue tracker has been disabled and the GitHub pull request
template mentions the new workflow, since it seems GitHub will not let me
disable pull requests.

If you don’t want to make any new bookmarks, the links to the main project
repository and bug tracker are linked at the top of every page on https://www.jetporch.com/.

Easy enough!

To file a bug in the web interface, you will need a free SourceHut account —
you do not need to pay them for hosting a repo, and if you want to keep
pushing your changes onto your private GitHub, there’s nothing preventing
that of course.

We’re also now a small monorepo.

Inventory scripts have been merged to “contrib/inventory” and example
content has been moved to “examples/” all in the same repo.

This makes it easier to take pull patches for modules that also want to
add examples at the same time (let’s do more of this!).

If you have an existing checkout and don’t want to re-clone, you can edit
your .git/config and change the origin address to https://git.sr.ht/~mpdehaan/jetporch.

There is no need for a SSH-style authenticated clone to contribute to
the project anymore since the patches travel over email.

I think everyone will find out that once set up the contribution workflow
(see https://www.jetporch.com/community/sourcehut) is much faster and less
of a context switch, **but the time it saves me making the same rebases
again and again (which can possibly introduce errors I really don’t want
to even think about) is absolutely enormous**, which means faster merging
and also more time to develop features and fix tickets myself.

The **kernel-style patch workflow is objectively superior**.

Anyway, if you see any stale references to GitHub in the docs (they probably
exist), let me know on Discord, and I’ll edit them away quickly.

If you have any git questions, I’m also happy to help, and of course the
same goes for understanding any aspect of the code as well.

Thanks for the continued interest in Jet, and we’ll keep you posted as
things evolve!



.. _jetporch_sourcehut_2023_10_05:

2023-10-05 **jetporch : Preparing To Move From GitHub To Sourcehut**
===========================================================================

- https://www.jetporch.com/community/sourcehut#sending-in-a-patch
- https://jetporch.substack.com/p/preparing-to-move-from-github-to (Preparing To Move From GitHub To Sourcehut)

Hi all,

Jet is preparing to move to Sourcehut for source code hosting and
collaboration purposes.

Details of how Sourcehut works and the reason for this are currently
written up here with instructions.

Sourcehut is pretty awesome, both easier to contribute to as well as much
easier for me to attend to patches.

My plan is to merge the inventory scripts (jetporch/jeti on GitHub) and
the examples repo into just one common repository to keep it simple,
so there aren’t multiple repos to setup and configure.

We will keep GitHub around as a read-only mirror and to collect Github-star
pixie dust, which we will perhaps one day try to reclaim for a stuffed
animal at the prize booth.

If there are any questions about this, message me on the #jet-development
channel on Discord.

If you are getting this message and have an existing pull request,
you’re fine to leave it open and there is no need to resubmit it.

If you want to though, try to submit your next PR over Sourcehut and we
can help with any git questions over chat if you have questions.
