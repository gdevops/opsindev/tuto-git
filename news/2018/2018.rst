
.. index::
   pair: git ; 2018



.. _git_2018:

===================
Git news 2018
===================


.. _git_2018_03:

2018-03 new tool : git-sizer
==============================

.. seealso::

   - https://github.com/github/git-sizer
   - https://www.developpez.com/actu/192311/GitHub-publie-un-nouvel-outil-nomme-git-sizer-afin-d-aider-les-mainteneurs-a-optimiser-leurs-depots/
