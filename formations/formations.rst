
.. index::
   pair: Git ; Formations
   pair: Git ; Sogilis
   pair: Git ; Delicious insigths


.. _git_formations:

================
Formations
================

.. figure:: ../humor/git_humour.png
   :align: center


.. _formation_sogilis:

Sogilis
=========

.. seealso::

   - http://sogilis.com/formation-git/
   - https://x.com/Sogilis
   - http://sogilis.com/blog/notre-workflow-git-pourquoi-comment/


.. _formation_delicious_insight:

Delicious insigths
====================

.. seealso::

   - https://delicious-insights.com/fr/formations/git-total/
   - https://x.com/delicioinsights
   - https://x.com/porteneuve

Flows/tutorials
===============

.. toctree::
   :maxdepth: 3

   flows/flows
