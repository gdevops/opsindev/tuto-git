
.. index::
   pair: Git ; Résumé


.. _branch_resume:

===========================================================
Résumé des commandes git pour travailler sur une branche
===========================================================





TL;DR
=======

::

    git checkout -b feature-XXX  master
    git push --set-upstream origin feature-XXX
    git add .
    git commit . -m "OK feature XXX "
    git push
    ...
    ...
    git checkout master
    git merge --no-ff feature-XXX
    git push origin master
    git push --delete origin feature-XXX
    git branch --delete feature-XXX


Création d'une branche feature-XXX
====================================

::

    git checkout -b feature-XXX  master


::

    Basculement sur la nouvelle branche 'feature-XXX '


git push --set-upstream origin feature-XXX
--------------------------------------------

::

    git push --set-upstream origin feature-XXX



git add .
------------

::

    git add .



git commit . -m "OK feature XXX "
-----------------------------------

::

     git commit . -m "OK feature XXX "


git push
----------


::

    git push



git checkout master
--------------------


::

     pvergain@UC004 git checkout master

::

    Basculement sur la branche 'master'
    Votre branche est à jour avec 'origin/master'.



git merge --no-ff feature-XXX
-------------------------------

::

    git merge --no-ff feature-XXX



git push origin master
------------------------

::

    git push origin master




git push --delete origin feature-XXX
--------------------------------------

::

    git push --delete origin feature-XXX



git branch --delete feature-XXX
---------------------------------

::

    git branch --delete feature-XXX
