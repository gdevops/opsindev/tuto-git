
.. index::
   pair: Git Flow; Example 1


.. _git_flow_example_1:

===================
Git flow example 1
===================

.. seealso::

   - https://jeffkreeftmeijer.com/git-flow/




git flow init
===============

::

    git flow init

::

    Which branch should be used for bringing forth production releases?
       - master
    Branch name for production releases: [master]
    Branch name for "next release" development: [develop]

    How to name your supporting branch prefixes?
    Feature branches? [feature/]
    Bugfix branches? [bugfix/]
    Release branches? [release/]
    Hotfix branches? [hotfix/]
    Support branches? [support/]
    Version tag prefix? []
    Hooks and filters directory? [/home/pvergain/projects/update_id3/.git/hooks]


::

    git branch

::

    * develop
      master


::

    git push --set-upstream origin develop

    X11 forwarding request failed on channel 0
    Total 0 (delta 0), réutilisés 0 (delta 0)
    remote:
    remote: To create a merge request for develop, visit:
    remote:   http://gitlab.srv.int.id3.eu/Informatique/update_id3/merge_requests/new?merge_request%5Bsource_branch%5D=develop
    remote:
    To gitlab.srv.int.id3.eu:Informatique/update_id3.git
     * [new branch]      develop -> develop
    La branche 'develop' est paramétrée pour suivre la branche distante 'develop' depuis 'origin'.


::

    git flow feature start authentification_29

::

    Basculement sur la nouvelle branche 'feature/authentification_29'

    Summary of actions:
    - A new branch 'feature/authentification_29' was created, based on 'develop'
    - You are now on branch 'feature/authentification_29'

    Now, start committing on your feature. When done, use:

         git flow feature finish authentification_29
