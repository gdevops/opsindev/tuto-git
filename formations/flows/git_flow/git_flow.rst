
.. index::
   pair: Git ; Flow


.. _git_flow:

================
Git flow (2010)
================

.. seealso::

   - https://jeffkreeftmeijer.com/git-flow/
   - https://github.com/danielkummer/git-flow-cheatsheet
   - http://nvie.com/posts/a-successful-git-branching-model/
   - https://github.com/petervanderdoes/gitflow-avh
   - :ref:`git_flow_avh_command`





A successful Git branching model
==================================

.. seealso::

   - http://nvie.com/posts/a-successful-git-branching-model/


In this post I present the development model that I’ve introduced for
some of my projects (both at work and private) about a year ago, and
which has turned out to be very successful.
I’ve been meaning to write about it for a while now, but I’ve never
really found the time to do so thoroughly, until now.
I won’t talk about any of the projects’ details, merely about the
branching strategy and release management.


.. figure:: git-model_2x.png
   :align: center


Examples
==========

.. toctree::
   :maxdepth: 3

   examples/examples
