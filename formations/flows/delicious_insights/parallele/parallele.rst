


.. index::
   pair: Delicious insights ; Parallèle
   pair: Workflow ; Feature branching
   pair: Conventions ; Nommage


.. _delicious_parallele:

===============================================
Développer des fonctionnalités en parallèle
===============================================

.. seealso::

   - https://delicious-insights.com/fr/articles/git-workflows-parallel-features/
   - :ref:`delicious_generalites`
   - :ref:`delicious_versions`

.. contents::
   :depth: 5


.. _intro_branches:

Introduction aux branches
============================

.. seealso::

   - :ref:`gitlab_issue_tracking`

Que nous travaillions seul ou à plusieurs, il est un réflexe important
à acquérir et qui consiste à isoler systématiquement chaque ensemble
de développement.

Cet isolement se fait à l’aide des **branches**.

L’inconvénient que cela pose dans un workflow centralisé, c’est
l’aspect mono-branche du dépôt partagé (souvent en SVN/CVS)
qui rend **difficile la parallélisation des tâches**.

Admettons que nous ayons au sein d’une équipe des groupes de
développeur·se·s travaillant chacun·e à un sous-ensemble fonctionnel
du projet :

- Estelle et Xavier sur *feature-1*,
- Jonathan et Marie sur *feature-2*.

Avec un dépôt central mono-branche unique, avoir une version stable à un
instant donné relève de l'impossible, puisque quand feature-1 sera
terminée, il est fort probable que feature-2 soit encore en cours de
développement et donc pollue le projet, empêchant un déploiement
en production.


.. figure:: workflows_centralized.png
   :align: center

   Workflow centralisé


En isolant alors le développement de chaque lot sur une branche dédiée,
on s’assure que les lots rapatriés sur la branche centrale/de production
(ici master) seront terminés.
Ce principe est couramment appelé `Feature Branching`_ car il encourage la
création d’une branche par fonctionnalité.


.. _`Feature Branching`:  https://martinfowler.com/bliki/FeatureBranch.html



.. _creation_feature_1:

Création d'une branche feature-1
---------------------------------

.. seealso::

   - :ref:`gitlab_issue_tracking`
   - :ref:`testflow1_feature_1`

Création de la branche *feature-1* par Estelle et partage **via le
dépôt distant.**

.. code-block:: bash
   :linenos:

   # création d'une branche feature-1 à partir de master
   git checkout -b feature-1 master
   # création de la branche sur le serveur distant
   # option courte : -u
   git push --set-upstream origin feature-1


Xavier récupère la nouvelle référence de branche pour pouvoir travailler
dessus également


.. code-block:: bash
   :linenos:

   git fetch origin
   git checkout feature-1
   # (développement de la tâche f1-X)
   git commit -am ‘Message concis décrivant f1-X’
   # (développement de la tâche f1-Y)
   git commit -am ‘Message concis décrivant f1-Y’
   # (une fois ses tâches terminées, en fin de journée ou quand le partage est nécessaire :)
   git push


Estelle et Xavier réalisent alors des tâches sur cette branche,
déclinant au besoin des sous-branches sur leur poste/en local.


Testflow1 feature_1
----------------------

.. toctree::
   :maxdepth: 3

   ../testflow1/feature_1/feature_1


.. _creation_feature_2:

Création d'une branche feature-2
---------------------------------

.. seealso::

   - :ref:`gitlab_issue_tracking`

En parallèle, Jonathan a également créé une branche locale.
Il commence à travailler dessus et produit quelques commits avant **de
la partager sur le dépôt central** pour que Marie puisse également
y participer


.. code-block:: bash
   :linenos:

    # création d'une branche feature-2 à partir de master
    git checkout -b feature-2 master
    # (développement de la tâche f2-A)
    git commit -am ‘Message concis décrivant f2-A’
    # (partage à Estelle :)
    git push --set-upstream origin feature-2 # option courte : -u


Marie récupère cette branche et y contribue.

.. code-block:: bash
   :linenos:

    git pull origin
    git checkout feature-2
    # (développement de la tâche f2-B)
    git commit -am ‘Message concis décrivant f2-B’


.. figure:: workflows_feature_branching.png
   :align: center

   Workflow feature branching


.. _merge_no_ff:

Delicious insights merge avec l'option no-ff (no fast-forward)
----------------------------------------------------------------

.. seealso::

   - :ref:`gitlab_issue_tracking`
   - :ref:`testflow1_merge_no_ff`


Une fois le travail terminé par l’un des deux groupes, disons feature-2
par Marie et Jonathan, cette branche pourra être fusionnée sur sa
branche de départ, à savoir **master**.

Marie se charge de ce travail.

.. code-block:: bash
   :linenos:

    git checkout master
    git merge --no-ff feature-2
    git push origin master


.. note::  l’option --no-ff force la conservation de la **bosse** de
   la branche feature-2 dans l’historique (à savoir son point de départ
   et d’arrivée depuis et dans master), à l’instar du **fast-forward**
   option par défaut (--ff).
   Cette option peut-être modifiée dans la configuration globale
   (git config --global merge.ff false) ou locale au projet
   (git config merge.ff false).



.. _del_feature_branch:

Delicious insights Suppression de la branche feature 2
---------------------------------------------------------

.. seealso::

   - :ref:`gitlab_issue_tracking`

Marie en profite également pour supprimer l’étiquette de branche locale
et son équivalent distant (cela ne supprime pas l’historique, juste
l’emplacement du nom de branche)



.. code-block:: bash
   :linenos:

    # Suppression de la branche distante feature-2
    git push --delete origin feature-2
    # # Suppression de la branche locale feature-2
    git branch --delete feature-2


Testflow1 feature_2
----------------------

.. toctree::
   :maxdepth: 3

   ../testflow1/feature_2/feature_2



.. _intro_nommer_branches:

Bien nommer ses branches
==========================

.. seealso::

   - :ref:`nommer_branches`
   - :ref:`historique_clair`


Pour ne pas nous perdre dans nos schémas de branches il est important
de définir des :ref:`conventions de nommage <nommer_branches>` tout comme nous
l'avions vu précédemment pour :ref:`nos commits <historique_clair>`.





Chapitres précédent/suivant
=============================

.. seealso::

   - :ref:`delicious_generalites`
   - :ref:`delicious_versions`
