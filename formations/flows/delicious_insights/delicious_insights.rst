


.. index::
   pair: Delicious insights ; Git


.. _delicious_git_tuto:

====================================
*Delicious insights* Git tutorial
====================================

.. seealso::

   - https://delicious-insights.com/fr/articles/git-workflows-generality/


.. toctree::
   :maxdepth: 5


   generality/generality
   parallele/parallele
   releases/releases
   fix_bugs/fix_bugs
   conventions/conventions
