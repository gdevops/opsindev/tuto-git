


.. _testflow1_create_versions:

====================================
testflow1 create versions
====================================

.. seealso::

   - :ref:`delicious_git_tuto`
   - :ref:`my_git_config`






Version 1.0.0 (2018-03-12)
============================

git checkout -b 1.0.0 master
-------------------------------

Add Changelog


git add .
-----------


git ci . -m "Add Changelog"
------------------------------



git push --set-upstream origin 1.0.0
-------------------------------------

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git push --set-upstream origin 1.0.0

::

	Décompte des objets: 3, fait.
	Delta compression using up to 4 threads.
	Compression des objets: 100% (3/3), fait.
	Écriture des objets: 100% (3/3), 315 bytes | 157.00 KiB/s, fait.
	Total 3 (delta 1), reused 0 (delta 0)
	remote:
	remote: To create a merge request for 1.0.0, visit:
	remote:   https://gitlab.com/pvergain/testflow1/merge_requests/new?merge_request%5Bsource_branch%5D=1.0.0
	remote:
	To gitlab.com:pvergain/testflow1.git
	 * [new branch]      1.0.0 -> 1.0.0
	La branche '1.0.0' est paramétrée pour suivre la branche distante '1.0.0' depuis 'origin'.


git checkout master
---------------------

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git checkout master

::


	Basculement sur la branche 'master'
	Votre branche est à jour avec 'origin/master'.
	pvergain@uc026:/mnt/c/projects_id3/testflow1$ git branch
	  1.0.0
	* master


git merge --no-ff 1.0.0
------------------------


::


	pvergain@uc026:/mnt/c/projects_id3/testflow1$ git merge --no-ff 1.0.0
	Merge made by the 'recursive' strategy.
	 Changelog | 5 +++++
	 1 file changed, 5 insertions(+)
	 create mode 100644 Changelog


git push
----------

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git push

::

	Décompte des objets: 1, fait.
	Écriture des objets: 100% (1/1), 231 bytes | 115.00 KiB/s, fait.
	Total 1 (delta 0), reused 0 (delta 0)
	To gitlab.com:pvergain/testflow1.git
	   bd3c428..228bb80  master -> master



.. figure:: branche_1.0.0.png
   :align: center




Version 2.0.0 (2018-03-13)
============================

::

	git co -b 2.0.0 master
	# modifications de Changelog, etc...
	git add .
	git ci . -m "Add Changelog"
	git push --set-upstream origin 2.0.0
	git co master
	git merge --no-ff 2.0.0
	git push



Version 2.1.0 (2018-03-13)
============================

::

	git co -b 2.1.0 2.0.0
	# modifications de Changelog, etc...
	git add .
	git ci . -m "Add Changelog"
	git push --set-upstream origin 2.1.0
	git co master
	git merge --no-ff 2.1.0
	git push



.. _version_2_2_0:

Version 2.2.0 (2018-03-13)
============================

::

	git co -b 2.2.0 2.1.0
	# modifications de Changelog, etc...
	git add .
	git ci . -m "Add Changelog"
	git push --set-upstream origin 2.2.0
	git co master
	git merge --no-ff 2.2.0
	git push




Version 3.0.0 (2018-03-14)
============================

::

	git co -b 3.0.0 master
	# modifications de Changelog, etc...
	git add .
	git ci . -m "Add Changelog"
	git push --set-upstream origin 3.0.0
	git co master
	git merge --no-ff 3.0.0
	git push

.. figure:: branche_3.0.0.png
   :align: center



Version 3.1.0 (2018-03-15)
============================

::

	git co -b 3.1.0 3.0.0
	# modifications de Changelog, etc...
	git add .
	git ci . -m "Add Changelog"
	git push --set-upstream origin 3.1.0
	git co master
	git merge --no-ff 3.1.0
	git push


git push --dry-run --set-upstream origin 3.1.0
-------------------------------------------------

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git push --dry-run --set-upstream origin 3.1.0

::

	To gitlab.com:pvergain/testflow1.git
	 * [new branch]      3.1.0 -> 3.1.0
	Positionnerait la branche amont de '3.1.0' sur '3.1.0' de 'origin'



git push  --set-upstream origin 3.1.0
---------------------------------------

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git push  --set-upstream origin 3.1.0


::

	Décompte des objets: 6, fait.
	Delta compression using up to 4 threads.
	Compression des objets: 100% (6/6), fait.
	Écriture des objets: 100% (6/6), 594 bytes | 198.00 KiB/s, fait.
	Total 6 (delta 4), reused 0 (delta 0)
	remote:
	remote: To create a merge request for 3.1.0, visit:
	remote:   https://gitlab.com/pvergain/testflow1/merge_requests/new?merge_request%5Bsource_branch%5D=3.1.0
	remote:
	To gitlab.com:pvergain/testflow1.git
	 * [new branch]      3.1.0 -> 3.1.0
	La branche '3.1.0' est paramétrée pour suivre la branche distante '3.1.0' depuis 'origin'.


git co master
--------------


git merge --no-ff 3.1.0
--------------------------

git push
-----------


.. _version_3_1_1:

Version 3.1.1 (2018-03-15)
============================

::

	git co -b 3.1.1 3.1.0
	# modifications de Changelog, etc...
	git add .
	git ci . -m "Add Changelog"
	git push --set-upstream origin 3.1.1
	git co master
	git merge --no-ff 3.1.1
	git push


git dlog
---------

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git dlog

::

	* c1110b6 - (HEAD -> 3.1.1) Fix 3.1.0 (il y a 20 secondes) <Patrick VERGAIN>
	* ca08a2c - (origin/3.1.0, 3.1.0) Update 3.1.0 step2 (il y a 36 minutes) <Patrick VERGAIN>
	* 736ce7a - Create version 3.1.0 (il y a 36 minutes) <Patrick VERGAIN>
	* 60ac8dd - (origin/3.0.0, 3.0.0) 3.0.0 OK (il y a 49 minutes) <Patrick VERGAIN>
	*   73ff5a4 - Merge branch '2.0.0' (il y a 60 minutes) <Patrick VERGAIN>
	|\
	| * 0c78ff0 - (origin/2.0.0, 2.0.0) 2.0.0 OK (il y a 66 minutes) <Patrick VERGAIN>
	| * 845b87e - version 2 (il y a 82 minutes) <Patrick VERGAIN>
	|/
	*   228bb80 - Merge branch '1.0.0' (il y a 86 minutes) <Patrick VERGAIN>
	|\
	| * ab53dcc - (origin/1.0.0, 1.0.0) Add Changelog (il y a 2 heures) <Patrick VERGAIN>
	|/
	*   bd3c428 - Merge branch 'feature-2' into 'master' (il y a 3 heures) <Vergain>
	|\
	| * 832f65c - Update feature 2 step3 (il y a 3 heures) <Patrick VERGAIN>
	| * 7b75abf - Update feature 2 step2 (il y a 3 heures) <Patrick VERGAIN>
	| * 3ba0cb3 - Update feature 2 step1 (il y a 3 heures) <Patrick VERGAIN>
	| * b723d6c - Begin implementing feature 2 (il y a 4 heures) <Patrick VERGAIN>
	|/
	*   005c357 - Merge branch 'feature-1' (il y a 6 heures) <Patrick VERGAIN>
	|\
	| * 3803afe - Add new comments on feature 1 (il y a 7 heures) <Patrick VERGAIN>
	| * 8e58300 - Add comments on feature 1 (il y a 7 heures) <Patrick VERGAIN>
	| * e5359ee - Implement feature1 (il y a 7 heures) <Patrick VERGAIN>
	|/
	* fa7ce3c - First commit (il y a 7 heures) <Patrick VERGAIN>



.. _git_rebase_3_1_1:

git rebase master 3.1.1
--------------------------

Entre temps on se rend compte qu'on a oublié de créer les versions 2.1.0
et 2.2.0.
On les crée et on fait un rebase de master sur 3.1.1.

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git rebase master 3.1.1

::

	Rembobinage préalable de head pour pouvoir rejouer votre travail par-dessus...
	Application de  Fix 3.1.0


.. _git_rebase_head_2:

git rebase -i HEAD~2 (Historique propre)
-----------------------------------------

.. figure:: git_rebase.png
   :align: center

   git rebase -i HEAD~2



git tag -a hotfix3.1.1 -m "Hotfix 3.1.1"
-------------------------------------------



git checkout -b hotfix-2.2.1 2.2.0
--------------------------------------

::

    git checkout -b hotfix-2.2.1 2.2.0


git cherry-pick -x hotfix3.1.1
---------------------------------

::

    git cherry-pick -x hotfix3.1.1



.. _git_cherry_pick_continue:

git cherry-pick --continue
----------------------------

::

	Sur la branche hotfix-2.2.1
	Vous êtes actuellement en train de picorer le commit b6e73c2.
	  (tous les conflits sont réglés : lancez "git cherry-pick --continue")
	  (utilisez "git cherry-pick --abort" pour annuler le picorage)

	Modifications qui seront validées :

			modifié :         Changelog


::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git cherry-pick --continue

::

	[hotfix-2.2.1 e02812a] Cheery-pick 2.2.0 Fix 3.1.0
	 Date: Thu Mar 15 16:48:40 2018 +0100
	 1 file changed, 23 insertions(+)


git merge --no-ff hotfix-2.2.1
--------------------------------

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git merge --no-ff hotfix-2.2.1

::

	Merge made by the 'recursive' strategy.
	Changelog | 23 +++++++++++++++++++++++
	1 file changed, 23 insertions(+)


git tag 2.2.1
--------------

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git tag 2.2.1


git branch -d hotfix-2.2.1
---------------------------

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git branch -d hotfix-2.2.1

::

    Branche hotfix-2.2.1 supprimée (précédemment e02812a).


git co 2.2.0
---------------

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git co 2.2.0

::

	Basculement sur la branche '2.2.0'
	Votre branche est en avance sur 'origin/2.2.0' de 2 commits.
	  (utilisez "git push" pour publier vos commits locaux)

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git push

::

	Décompte des objets: 4, fait.
	Delta compression using up to 4 threads.
	Compression des objets: 100% (4/4), fait.
	Écriture des objets: 100% (4/4), 656 bytes | 109.00 KiB/s, fait.
	Total 4 (delta 2), reused 0 (delta 0)
	remote:
	remote: To create a merge request for 2.2.0, visit:
	remote:   https://gitlab.com/pvergain/testflow1/merge_requests/new?merge_request%5Bsource_branch%5D=2.2.0
	remote:
	To gitlab.com:pvergain/testflow1.git
	   6d0b2c5..8ad7236  2.2.0 -> 2.2.0
