


.. _testflow1_feature1:

====================================
testflow1 feature 1
====================================

.. seealso::

   - :ref:`delicious_git_tuto`
   - :ref:`my_git_config`






.. _testflow1_feature_1:

Testflow1 git checkout -b feature-1 master
=============================================

.. seealso::

   - :ref:`creation_feature_1`


::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git checkout -b feature-1 master

::

    Basculement sur la nouvelle branche 'feature-1'


git status
===========

::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git status

::

    Sur la branche feature-1
    rien à valider, la copie de travail est propre


git push
=========

::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git push

::

    fatal: La branche courante feature-1 n'a pas de branche amont.
    Pour pousser la branche courante et définir la distante comme amont, utilisez

        git push --set-upstream origin feature-1


git push --set-upstream origin feature-1
===========================================

::

    Décompte des objets: 12, fait.
    Delta compression using up to 4 threads.
    Compression des objets: 100% (9/9), fait.
    Écriture des objets: 100% (12/12), 1.43 KiB | 208.00 KiB/s, fait.
    Total 12 (delta 4), reused 0 (delta 0)
    remote:
    remote: To create a merge request for feature-1, visit:
    remote:   https://gitlab.com/pvergain/testflow1/merge_requests/new?merge_request%5Bsource_branch%5D=feature-1
    remote:
    To gitlab.com:pvergain/testflow1.git
     * [new branch]      feature-1 -> feature-1
    La branche 'feature-1' est paramétrée pour suivre la branche distante 'feature-1' depuis 'origin'.



.. figure:: branch_feature_1.png
   :align: center

   New feature-1 branch


.. _testflow1_merge_no_ff:

Testflow1 Merge avec l'option no-ff (no fast-forward)
========================================================

.. seealso::

   - :ref:`merge_no_ff`


git checkout master
--------------------

::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git checkout master

::

    Basculement sur la branche 'master'
    Votre branche est à jour avec 'origin/master'.


git branch
------------

::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git branch

::

      feature-1
    * master

git merge --no-ff feature-1
-------------------------------

::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git merge --no-ff feature-1



.. figure:: commit_for_merge_feature1_with_master.png
   :align: center

   The commit message


::


    Merge made by the 'recursive' strategy.
     feature1/README.rst | 56 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     1 file changed, 56 insertions(+)
     create mode 100644 feature1/README.rst

git status
------------

::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git st

::

    Sur la branche master
    Votre branche est en avance sur 'origin/master' de 4 commits.
      (utilisez "git push" pour publier vos commits locaux)

    rien à valider, la copie de travail est propre



.. figure:: git_dlog_feature_1.png
   :align: center

   git dlog


git push
----------

::

    git push


::

    Décompte des objets: 1, fait.
    Écriture des objets: 100% (1/1), 234 bytes | 117.00 KiB/s, fait.
    Total 1 (delta 0), reused 0 (delta 0)
    To gitlab.com:pvergain/testflow1.git
       fa7ce3c..005c357  master -> master


.. figure:: git_push_feature_1.png
   :align: center

   git push


.. figure:: activite_feature_1.png
   :align: center


Delete the feature_1 branch
===============================

git push --delete origin feature-1
--------------------------------------

::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git push --delete origin feature-1

::

    To gitlab.com:pvergain/testflow1.git
     - [deleted]         feature-1



git branch --delete feature-1
--------------------------------

::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git branch --delete feature-1

::

    Branche feature-1 supprimée (précédemment 3803afe).


Logs
=======

View graph with the command line interface (git dlog)
---------------------------------------------------------

::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git dlog

::

	*   005c357 - (HEAD -> master, origin/master) Merge branch 'feature-1' (il y a 54 minutes) <Patrick VERGAIN>
	|\
	| * 3803afe - Add new comments on feature 1 (il y a 80 minutes) <Patrick VERGAIN>
	| * 8e58300 - Add comments on feature 1 (il y a 2 heures) <Patrick VERGAIN>
	| * e5359ee - Implement feature1 (il y a 2 heures) <Patrick VERGAIN>
	|/
	* fa7ce3c - First commit (il y a 2 heures) <Patrick VERGAIN>



View graph with the web application (gitlab)
----------------------------------------------


.. figure:: graph_feature_1.png
   :align: center



View graph with the desktop GUI application (tortoisegit)
----------------------------------------------------------

.. seealso::

   - :ref:`tortoisegit`


.. figure:: tortoisegit_merge_testflow1.png
   :align: center

   View graph with tortoisegit
