
.. index::
   pair: Merge request ; feature_2
   pair: MR ; feature_2

.. _testflow1_feature2:

====================================
testflow1 feature 2
====================================

.. seealso::

   - :ref:`delicious_git_tuto`
   - :ref:`my_git_config`





git checkout -b feature-2 master
==================================

.. figure:: create_feature_2.png
   :align: center


::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git checkout -b feature-2 master

::

    Basculement sur la nouvelle branche 'feature-2'

::

	(tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ mkdir feature_2
	(tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ cd feature
	bash: cd: feature: Aucun fichier ou dossier de ce type
	(tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ cd feature_2
	(tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1/feature_2$ touch feature_2.rst
	(tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1/feature_2$ cd ..



git status
===========

::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git st

::

	Sur la branche feature-2
	Fichiers non suivis:
	  (utilisez "git add <fichier>..." pour inclure dans ce qui sera validé)

			feature_2/

	aucune modification ajoutée à la validation mais des fichiers non suivis sont présents (utilisez "git add" pour les suivre)

git add .
===========

::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git add .


git status
===========

::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git st

::

	Sur la branche feature-2
	Modifications qui seront validées :
	  (utilisez "git reset HEAD <fichier>..." pour désindexer)

			nouveau fichier : feature_2/feature_2.rst

::

    git ci . -m "Begin implementing feature 2"

::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git ci . -m "Begin implementing feature 2"

::

	[feature-2 b723d6c] Begin implementing feature 2
	 1 file changed, 9 insertions(+)
	 create mode 100644 feature_2/feature_2.rst



git push --set-upstream origin feature-2
==========================================

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git push --set-upstream origin feature-2

::

	Décompte des objets: 12, fait.
	Delta compression using up to 4 threads.
	Compression des objets: 100% (9/9), fait.
	Écriture des objets: 100% (12/12), 1013 bytes | 144.00 KiB/s, fait.
	Total 12 (delta 5), reused 0 (delta 0)
	remote:
	remote: To create a merge request for feature-2, visit:
	remote:   https://gitlab.com/pvergain/testflow1/merge_requests/new?merge_request%5Bsource_branch%5D=feature-2
	remote:
	To gitlab.com:pvergain/testflow1.git
	 * [new branch]      feature-2 -> feature-2
	La branche 'feature-2' est paramétrée pour suivre la branche distante 'feature-2' depuis 'origin'.



merge request
===============

.. seealso::

   - https://gitlab.com/help/user/project/merge_requests/index.md#checkout-merge-requests-locally
   - https://gitlab.com/pvergain/testflow1/merge_requests/1


.. figure:: merge_request.png
   :align: center

   Merge request page



.. figure:: merge_request_discussion.png
   :align: center

   Merge request discussion


.. figure:: merge_request_update.png
   :align: center

   Merge request discussion


.. figure:: merge_request_manuel.png
   :align: center

   Merge request discussion


.. figure:: merge_request_end.png
   :align: center

   Merge request discussion




.. figure:: feature_2_has_been_merged.png
   :align: center



Git checkout master
=====================

De retour dans le développement.


Git pull -u
=============

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git pull

::

	remote: Counting objects: 1, done.
	remote: Total 1 (delta 0), reused 0 (delta 0)
	Dépaquetage des objets: 100% (1/1), fait.
	Depuis gitlab.com:pvergain/testflow1
	   005c357..bd3c428  master     -> origin/master
	Mise à jour 005c357..bd3c428
	Fast-forward
	 feature_2/feature_2.rst | 29 +++++++++++++++++++++++++++++
	 1 file changed, 29 insertions(+)
	 create mode 100644 feature_2/feature_2.rst


Logs
=====


View graph with the command line interface (git dlog)
---------------------------------------------------------

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git dlog

::

	*   bd3c428 - (HEAD -> master, origin/master) Merge branch 'feature-2' into 'master' (il y a 12 minutes) <Vergain>
	|\
	| * 832f65c - (origin/feature-2, feature-2) Update feature 2 step3 (il y a 44 minutes) <Patrick VERGAIN>
	| * 7b75abf - Update feature 2 step2 (il y a 49 minutes) <Patrick VERGAIN>
	| * 3ba0cb3 - Update feature 2 step1 (il y a 50 minutes) <Patrick VERGAIN>
	| * b723d6c - Begin implementing feature 2 (il y a 2 heures) <Patrick VERGAIN>
	|/
	*   005c357 - Merge branch 'feature-1' (il y a 4 heures) <Patrick VERGAIN>
	|\
	| * 3803afe - Add new comments on feature 1 (il y a 4 heures) <Patrick VERGAIN>
	| * 8e58300 - Add comments on feature 1 (il y a 4 heures) <Patrick VERGAIN>
	| * e5359ee - Implement feature1 (il y a 5 heures) <Patrick VERGAIN>
	|/
	* fa7ce3c - First commit (il y a 5 heures) <Patrick VERGAIN>



View graph with the web application (gitlab)
----------------------------------------------


.. figure:: master_graph.png
   :align: center

git branch
============

The feature_2 branch does exist.

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git branch

::

	  feature-2
	* master



Delete the feature_2 branch
===============================

git push --delete origin feature-1
--------------------------------------

::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git push --delete origin feature-2

::

	To gitlab.com:pvergain/testflow1.git
	 - [deleted]         feature-2


git branch --delete feature-1
--------------------------------

::

    (tuto_git-QpDXWaZY) pvergain@uc026:/mnt/c/projects_id3/testflow1$ git branch --delete feature-2


::

	pvergain@uc026:/mnt/c/projects_id3/testflow1$ git branch --delete feature-2
	Branche feature-2 supprimée (précédemment 832f65c).
