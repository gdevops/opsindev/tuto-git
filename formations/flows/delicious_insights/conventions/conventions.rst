


.. index::
   pair: Delicious insights ; Conventions
   pair: Branches ; depuis master
   pair: Branches ; depuis production
   pair: Coding ; Style
   pair: WIP ; Work In Progress
   pair: Ticket ; Issue


.. _delicious_conventions:

===============================================
Définir les conventions d’un projet
===============================================

.. seealso::

   - https://delicious-insights.com/fr/articles/git-workflows-conventions/
   - :ref:`delicious_fix_bugs`
   - :ref:`delicious_generalites`


.. contents::
   :depth: 7


Introduction
===============

La définition ou l’utilisation de **conventions** au sein d’une équipe,
d’une entreprise ou d’une communauté favorise la pérénité des projets
qui y adhèrent.

Les bénéfices sont multiples:

- On uniformise les travaux effectués ;
- On favorise la lecture et l'analyse de l'historique du projet ;
- On fournit un socle pour automatiser certaines tâches ;
- On réduit le coût d’entrée/d’apprentissage du contexte projet pour un
  acteur connaissant ces conventions ;
- On optimise la création et la gestion de nouveaux projets basés sur
  des règles existantes ;


.. _lecture_historique:

Faciliter la lecture de l’historique
======================================

.. seealso::

   - :ref:`commit_right_message`
   - http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html

Commits
---------

.. seealso::

   - https://github.com/conventional-changelog/conventional-changelog
   - https://github.com/commitizen/cz-cli
   - https://github.com/marionebl/commitlint


Le bon nommage des messages de commits est souvent un signe de qualité
de la gestion du projet et du bon découpage des tâches.

Le but est de définir un schéma logique d'écriture de ces messages,
car Git n'impose aucune contrainte.

On trouve de nombreuses initiatives qui proposent des normalisations.

L’une des plus connues est probablement le `conventional changelog`_.

Elle consiste à fournir en première ligne une description concise de
la tâche couverte par le commit, puis des informations complémentaires
dans les paragraphes suivants.

On facilite ainsi la lecture du log d’une part, la génération
automatique du changelog (ou release notes) d’autre part, en listant
la première ligne de chaque message d'un point A de l'historique à un
point B (en général ces points sont définis par des tags).

Voici un squelette d’exemple de message de commit conventionnel::

    <type de tâche>(<périmètre>): message court
    Description complémentaire/complète
    Référence/action sur un ticket définissant cette tâche

.. _`conventional changelog`:  https://github.com/conventional-changelog/conventional-changelog



On obtient alors des historiques faciles à analyser::

    git log --graph --oneline -12


::

    * 3855f12 fix(ci): restrict Jest to app/assets tests (void vendor/bundle stuff)
    * c872d19 fix(tests): restore working JS tests
    * d57ef3b chore(docs): Annotated source with groc for JS code
    * 9600601 feat(uploads): finalized React components for dialog-based uploading
    * 74b72c1 infra(uploads): add `extraClass` to `<Button/>` and `<SubmitButton/>`
    * 9399973 chore(js): expose admin L10n to React
    * aa96ea2 chore(uploads): add Semantic UI’s dimmer/modal modules
    * 2be485f chore(uploads): upgrade react-s3-uploader to prune factory warnings
    * 5eb190d chore(console): Add awesome_print. Require it in your `.pryrc` for `ap`
    * 18109a4 test(uploads): add missing test coverage for recent devs
    * f73eb02 feat(uploads): upload creation (and last-minute S3 rename) action
    * d43d8f7 feat(uploads): S3 upload forms now handle abort-and-remove behaviors


On peut alors imaginer créer un aperçu des changements effectués sur le
projet (release notes) en regroupant les commits par thématique::

    New features
    -------------

    # Uploads

    - S3 upload forms now handle abort-and-remove behaviors
    - Upload creation (and last-minute S3 rename) action


    Patches
    -------
    - Restore working JS tests
    - Restrict Jest to app/assets tests (void vendor/bundle stuff)


    Chore updates
    -------------
    - Add awesome_print. Require it in your `.pryrc` for `ap`



Des outils tels que commitizen_ et commitlint_ peuvent vous assister
dans la construction de vos messages de commit.



.. _commitizen:  https://github.com/commitizen/cz-cli
.. _commitlint:  https://github.com/marionebl/commitlint

.. _nommer_branches:

Nommer les branches
=====================

.. seealso::

   - https://about.gitlab.com/2014/09/29/gitlab-flow/
   - :ref:`intro_nommer_branches`


Nous avons déjà marqué notre souhait de d’identifier les sous-ensembles
de développement en utilisant :ref:`des branches <intro_branches>`.

Au même titre que pour les commits, l’organisation et la lecture de
celles-ci sera facilitée si leurs **noms sont conventionnels**.

Voici un exemple de convention basé sur le `GitLab flow`_ :

- Branche de développement principale : **master**.
- Branche de pré-production pour tests fonctionnels et intégration: **pre-production**
- Autres branches : **<contexte>/(<périmètre>/)<fonction>** (périmètre optionnel):

    - **Branches de fonctionnalités créées depuis master** : *contexte feat*

      Exemple: **feat/ux/a11y**:

          - périmètre *expérience utilisateur*,
          - fonction *accessibilité*.


    - **Branches de correctifs créées depuis production** : *contexte hotfix*

      Exemple: **hotfix/42-form-submit** : ici un numéro de ticket est utilisé
      avec un nom décrivant la fonction impactée (le périmètre n’est pas nécessaire).


.. warning::  L’utilisation du délimiteur **/** dans les noms de
   branches ne vous permettra pas avec Git d’utiliser les noms
   intermédiaires.

Par exemple si vous créez une branche feat/ux/a11y, vous ne pourrez pas
créer de branche feat/ux.
Préférez alors un autre séparateur : feat-ux, feat-ux-a11y.


.. _`GitLab flow`:  https://about.gitlab.com/2014/09/29/gitlab-flow/


Exemples de nommage
----------------------

git checkout -b feature/devops/ansible-4 master
--------------------------------------------------

.. figure:: feature_ansible_devop_4.png
   :align: center

   Déclaration de la feature devops 4 dans gitlab

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git co -b feature/devops/ansible-4 master

::


    Basculement sur la nouvelle branche 'feature/devops/ansible-4'


git branch
+++++++++++++

::

    pvergain@uc026:/mnt/c/projects_id3/testflow1$ git branch

::

	  1.0.0
	  2.0.0
	  2.1.0
	  2.2.0
	  3.0.0
	  3.1.0
	  3.1.1
	* feature/devops/ansible-4
	  master



git push --set-upstream origin feature/devops/ansible-4
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Après ajout des rôles Ansible on pousse la feature sur le serveur
distant.


::

    git push --set-upstream origin feature/devops/ansible-4

::

	Décompte des objets: 131, fait.
	Delta compression using up to 4 threads.
	Compression des objets: 100% (101/101), fait.
	Écriture des objets: 100% (131/131), 33.44 KiB | 349.00 KiB/s, fait.
	Total 131 (delta 15), reused 0 (delta 0)
	remote: Resolving deltas: 100% (15/15), completed with 1 local object.
	remote:
	remote: To create a merge request for feature/devops/ansible-4, visit:
	remote:   https://gitlab.com/pvergain/testflow1/merge_requests/new?merge_request%5Bsource_branch%5D=feature%2Fdevops%2Fansible-4
	remote:
	To gitlab.com:pvergain/testflow1.git
	 * [new branch]      feature/devops/ansible-4 -> feature/devops/ansible-4
	La branche 'feature/devops/ansible-4' est paramétrée pour suivre la branche distante 'feature/devops/ansible-4' depuis 'origin'.



.. _merge_feature_devops_4:

Merge request feature/ansible/devops_4 from gitlab
-----------------------------------------------------



.. figure:: merge_request_feature_ansible_devops_4.png
   :align: center

   Merge request feature dev 4



.. figure:: create_merge_request.png
   :align: center

   https://gitlab.com/pvergain/testflow1/merge_requests/2


On accepte la merge request sur le serveur distant (gitlab)
--------------------------------------------------------------


.. figure:: merge_request_feature_ansible_devops_4_ok.png
   :align: center

.. figure:: after_merge_request_feature_ansible_devops_4_ok.png
   :align: center

.. figure:: commits_merge_request_feature_ansible_devops_4.png
   :align: center

   https://gitlab.com/pvergain/testflow1/commits/master


.. _git_pull_exemple:

Ne pas oublier git pull sur sa copie locale
----------------------------------------------

.. seealso::

   - :ref:`git_pull`


.. warning:: Ne pas oublier de faire un git pull sur sa copie locale
   après un git merge sur le serveur distant (gitlab)


::

    git pull



.. _versions_logicielles:

Versions logicielles
======================

.. seealso::

   - :ref:`semver`


Les ensembles d’évolutions d’un projet peuvent être identifiés à l'aide
des tags (de préférence annotés).

Nous avions vu :ref:`précédemment le Semantic Versioning <semver>`
qui est une convention très répandue.

Ces noms identifiables permettent dès lors des automatisations telles
que la génération automatique d'un changelog entre deux versions
majeures ou mineures.


Style de code
================

.. seealso::

   - https://developer.mozilla.org/fr/docs/Developer_Guide/Coding_Style
   - https://docs.gitlab.com/ce/user/project/merge_requests/work_in_progress_merge_requests.html


L’uniformisation d’un projet passe également par des choix stylistiques
et structurels au sein des fichiers sources, ainsi que leurs règles
de nommage.

Et comme **on ne réinvente pas la roue**, on peut utiliser des conventions
de style déjà existantes :

- `Coding Style de Mozilla`_
- `Google style guides`_
- et bien d’autres…

Vous pouvez également encourager l’utilisation d’outils pour analyser
le code, voire automatiser ces analyses :

- **Linters** : analyse statique de code (ex : StandardJS_, BuddyJS_,
  Prettier_, JsBeautifier_ pour JavaScript)
- **Code quality** : algorithmes, fuites mémoires… (ex : SonarQube_,
  `Code Climate`_, Snyk_, Plato_, Stylelint_)



.. _`Coding Style de Mozilla`:  https://developer.mozilla.org/fr/docs/Developer_Guide/Coding_Style
.. _`Google style guides`:  https://github.com/google/styleguide
.. _StandardJS:  https://standardjs.com/
.. _BuddyJS: https://github.com/danielstjules/buddy.js/
.. _Prettier: https://prettier.io/
.. _JsBeautifier: http://jsbeautifier.org/
.. _SonarQube: https://www.sonarqube.org/
.. _`Code Climate`: https://codeclimate.com/
.. _Snyk: https://snyk.io/
.. _Plato: https://github.com/es-analysis/plato
.. _Stylelint: https://stylelint.io/


Gestion de projet
===================

.. seealso::

   - https://docs.gitlab.com/ce/user/project/merge_requests/work_in_progress_merge_requests.html
   - https://docs.gitlab.com/ce/user/project/
   - https://gitlab.com/help/user/project/description_templates.md#setting-a-default-template-for-issues-and-merge-requests

Le cycle de vie d'un projet implique de définir des objectifs
fragmentables en tâches qui seront affectées aux différents intervenants.

La définition des **tâche**s constituant un projet peut aujourd'hui être
directement intégrée dans les interfaces de serveur Git (GitHub, GitLab,
Bitbucket).
On crée alors un **ticket** ou **issue** pour chaque **tâche**.

Certains systèmes vont même au-delà de la simple définition de tickets
en offrant des interfaces de gestion de projet avancées
(échéances/milestones, dates butoirs, étiquettes/labels, kanban board...).
Ceci évite la multiplication des logiciels et des éventuelles
passerelles entre ceux-ci.

Qu'ils soient intégrés ou non dans votre serveur Git, vos tickets
peuvent suivre des **conventions de nommage**, de rédaction,
d’organisation (labelling).

On peut imaginer un découpage pour normaliser l’écriture des demandes (issues) :

- Demande de nouvelle fonctionnalité (ex: étiquette Fonctionnalité)
- Demande de correctif (ex : étiquette Bug)

Il en va de même pour les **pull requests**.

Exemple : utilisation du préfixe WIP pour les pull requests dont le
travail n’est pas encore terminé (jetez un œil à la gestion GitLab
des WIP_ merge requests.

Selon le serveur Git que vous utiliserez vous aurez parfois la
possibilité d'éditer des gabarits d’issues et/ou de pull requests :

- GitHub : `issue and pull request template`_
- GitLab : `issues and merge requests templates`_


.. _WIP: https://docs.gitlab.com/ce/user/project/merge_requests/work_in_progress_merge_requests.html
.. _`issue and pull request template`:  https://github.com/blog/2111-issue-and-pull-request-templates
.. _`issues and merge requests templates`:  https://gitlab.com/help/user/project/description_templates.md#setting-a-default-template-for-issues-and-merge-requests


Documentation
===============

.. seealso::

   - https://pvergain.gitlab.io/pvbookmarks/index.html


L'organisation des fichiers participant à la documentation de vos
projets aidera à l'immersion d'un nouvel acteur en lui permettant
de retrouver facilement les informations utiles.

Des conventions existent pour certains noms de fichiers. Ceux-ci sont
souvent suggérés dans les interfaces des serveurs Git qui proposent
parfois des gabarits automatiques.

Voici les exemples d'aides de GitLab :

.. figure:: workflows_conventions_files_gitlab-1.png
   :align: center

   Workflow convention gitlab


À la création du projet, suggestion de création des fichiers

README.md
-----------

Les fichiers README.md peuvent être placés dans les différents
répertoires de vos projets et apporter des informations utiles
sur la fonction du projet ou d'un sous-ensemble.

Dans la plupart des interfaces de serveur Git, il est automatiquement
affiché comme page de présentation du projet ou du sous-dossier.


INSTALL.md
-------------

Lorsque cela s'avère nécessaire, la procédure d'installation du projet
doit être documentée.
On peut la séparer de la présentation du projet dans un fichier
INSTALL.md qui sera référencé depuis le README.md.

CONVENTIONS.md
-----------------

On y retrouve la liste des conventions du projet:

- Historique Git : nommage des commits, des branches et tags ;
- Coding Style : syntaxes à réspecter par langage ;
- Gestion de projet : nommage des issues et pull requests.

Ce contenu est souvent présent dans le fichier CONTRIBUTING.md et ne
nécessite alors pas la présence du fichier CONVENTIONS.md.

CONTRIBUTING.md
------------------

Il s'agit des règles à suivre pour contribuer au projet. Il pourrait
aussi bien s'appeler WORKFLOW.md ou PROCESS.md (que l’on trouve parfois
en complément).

On retrouve très souvent ce fichier dans le monde de l'open-source.
Les mainteneurs d'un projet y décrivent les règles à suivre pour
participer au projet de manière à ne pas devoir les répéter
systématiquement.

Cela peut tout à fait être transposé dans un projet privé.

On peut y insérer un lien vers les conventions, puisqu'elles font
partie intégrante des règles d'un projet, et pourquoi pas un lien
vers cette série d’articles.

Quelques exemples de procédures de contribution :

- GitLab:  `Gitlab contribution guidelines`_
- Node.js `Node.js contribution guidelines`_
- Rails `Rails contribution guidelines`_
- Atom `Atom contribution guidelines`_

.. _`Gitlab contribution guidelines`: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md
.. _`Node.js contribution guidelines`: https://github.com/nodejs/node/blob/master/CONTRIBUTING.md
.. _`Rails contribution guidelines`: https://github.com/rails/rails/blob/master/CONTRIBUTING.md
.. _`Atom contribution guidelines`: https://github.com/atom/atom/blob/master/CONTRIBUTING.md



CHANGELOG.md
--------------

Ce fichier décrit les évolutions successives d'un projet (entre versions).

Il existe des systèmes permettant d'automatiser la génération des release notes :

- `GitHub Releases`_
- Vandamme_


.. _`GitHub Releases`:  https://help.github.com/articles/creating-releases/
.. _Vandamme: http://tech-angels.github.io/vandamme/


LICENSE.md
-------------

On retrouve ce fichier principalement dans l'open source puisqu'il
décrit les droits d'exploitation du projet. Il n'a pas d'intérêt
dans un projet privé.

Certains serveur Git intégrent les différents gabarits de licences
open-source.


Et maintenant ?
=================

Si vous êtes arrivés à la fin de notre série d’articles sur les
workflows, vous êtes parés pour mettre en place vos propres workflows.

Ne vous inquiétez pas de la complexité, soyez progressifs et
demandez-vous quelle prochaine étape vous apportera le plus de confort
et de qualité.


Chapitres précédent/suivant
=============================

.. seealso::

   - :ref:`delicious_fix_bugs`
   - :ref:`delicious_generalites`
