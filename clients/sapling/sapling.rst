.. index::
   pair: git ; sapling

.. _sapling:

=======================================================================================
**sapling** (a cross-platform, highly scalable, Git-compatible source control system)
=======================================================================================

- https://github.com/facebook/sapling
- https://sapling-scm.com/
- https://sapling-scm.com/docs/introduction/getting-started/
- https://engineering.fb.com/2022/11/15/open-source/sapling-source-control-scalable/
- https://opensource.fb.com/


Using Sapling
===================

To start using Sapling, see the Getting Started page for how to clone
your existing Git repositories.

Checkout the Overview for a peek at the various features.
Coming from Git? Checkout the Git Cheat Sheet.

Sapling also comes with an Interactive Smartlog (ISL) web UI for seeing
and interacting with your repository, as well as a VS Code integrated
Interactive Smartlog.
