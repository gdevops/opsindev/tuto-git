.. Git tutorial documentation master file, created by
   sphinx-quickstart on Mon Mar 12 09:58:03 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>



.. figure:: _static/Git-logo.svg.png
   :align: center

   Logo Git


.. 🧐

|FluxWeb| `RSS <https://gdevops.frama.io/opsindev/tuto-git/rss.xml>`_

.. _git_tuto:
.. _tuto_git:

================
**Tuto git**
================

- https://en.wikipedia.org/wiki/Git
- https://git-scm.com/
- https://github.com/git/git (miroir)
- https://firstaidgit.io/
- :ref:`git_master_to_main`
- :ref:`howto_github_pull_request`
- :ref:`git_flow_memo`



.. toctree::
   :maxdepth: 6

   news/news
   definition/definition
   why/why
   installation/installation
   configuration/configuration
   commands/commands
   clients/clients
   tutorials/tutorials
   formations/formations
   cheatsheets/cheatsheets
   howto/howto
   humor/humor
   tools/tools

.. toctree::
   :maxdepth: 3
   :caption: Move from master to main


   master_to_main/master_to_main


.. toctree::
   :maxdepth: 4

   versions/versions
