
.. index::
   pair: git; extras

.. _ref_git_extras:

====================================
git-extras (Awesome GIT utilities)
====================================

.. seealso::

  - https://github.com/tj/git-extras
  - https://github.com/tj/git-extras/blob/master/Installation.md
  - https://github.com/tj/git-extras/blob/master/Commands.md
  - :ref:`zsh_git_extras`



git extras
===========


.. literalinclude:: help_git_extras.txt
   :linenos:


oh my zsh plugin
==================

.. seealso::

   - :ref:`oh_myzsh_git_extras`
