
.. index::
   pair: git flow; completion


.. _git_flow_completion:

===============================
git flow completion
===============================

.. seealso::

   - https://github.com/petervanderdoes/git-flow-completion




Description
============

Bash, Zsh and fish completion support for git-flow (AVH Edition).

The contained completion routines provide support for completing:

- git-flow init and version
- feature, hotfix and release branches
- remote feature, hotfix and release branch names
