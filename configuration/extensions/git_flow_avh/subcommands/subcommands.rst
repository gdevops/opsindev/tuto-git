
.. index::
   pair: git ; flow subcommands


.. _git_flow_sub_commands:

=======================
git flow sub commands
=======================

.. seealso::

   - https://jeffkreeftmeijer.com/git-flow/
   - https://github.com/petervanderdoes/gitflow-avh
   - https://github.com/danielkummer/git-flow-cheatsheet
   - :ref:`git_flow`





Initialize Git flow in a project
===================================

::

    git flow init

To start a new development branch
=====================================

::

    git flow feature start dev_branch_name

is equivalent to

::

    git checkout -b dev_branch_name dev

To finish an existing development branch
===========================================

::

    git flow feature finish dev_branch_name

is equivalent to

::

    git checkout dev
    git merge dev_branch_name --no-ff
    git branch -d dev_branch_name

You can do the same with releases and hotfixes.

::

    git flow release start release_branch_name
    git flow release finish release_branch_name
    git flow hotfix start release_branch_name
    git flow hotfix finish release_branch_name

For release and hotfixes, the equivalent are

::

    git checkout -b new_branch_name master

and

::

    git checkout dev
    git merge new_branch_name --no-ff
    git checkout master
    git merge new_branch_name --no-ff
    git tag v1.0
    git branch -d dev_branch_name
