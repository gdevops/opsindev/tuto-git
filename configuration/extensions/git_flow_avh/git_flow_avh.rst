
.. index::
   pair: git ; flow
   pair: AVH ; git flow


.. _git_flow_avh_command:

===============================
git flow command (AVH Edition)
===============================

.. seealso::

   - https://github.com/petervanderdoes/gitflow-avh
   - https://github.com/danielkummer/git-flow-cheatsheet
   - https://nvie.com/posts/a-successful-git-branching-model
   - :ref:`git_flow`


Description
============

AVH Edition of the git extensions to provide high-level repository
operations for `Vincent Driessen's branching model`_


.. _`Vincent Driessen's branching model`:  https://nvie.com/posts/a-successful-git-branching-model/


.. toctree::
   :maxdepth: 3


   installation/installation
   subcommands/subcommands
   git_flow_completion/git_flow_completion
   examples/examples
