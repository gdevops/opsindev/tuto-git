
.. index::
   pair: Git ; gitconfig

.. _gitconfile:

===================
gitconfig file
===================

- https://delicious-insights.com/fr/articles/configuration-git/
- https://jvns.ca/blog/2024/02/16/popular-git-config-options/


.. _my_git_config:

My ~/gitconfig
================

::

    (tuto_git-QpDXWaZY) $ cat ~/.gitconfig


    [user]
        name = Patrick VERGAIN
        email = patrick.vergain@id3.eu
    [core]
        editor = vim
    [push]
        default = simple
    [alias]
        dlog = log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
        ci = commit
        co = chekout
        st = status



.. _git_credential_cache_with_timeout:

git config --global credential.helper 'cache --timeout=28800'
================================================================

Pour ne pas avoir à taper son username/password à chaque push ou pull.

::

    git config --global credential.helper 'cache --timeout=28800'



.. _git_credential_cache:

git config --global credential.helper cache (without timeout)
================================================================


without timeout::

    git config --global credential.helper cache




Christophe Porteneuve configuration
=====================================


.. seealso::

   - https://gist.github.com/tdd/470582


.. literalinclude:: examples/exemple_configuration.txt
   :linenos:
