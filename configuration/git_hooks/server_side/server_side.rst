
.. index::
   pair: Git ; Server-side Hooks


.. _server_git_hooks:

=======================
Server-side git hooks
=======================

.. seealso::

   - https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
   - https://git-scm.com/book/en/v2/Customizing-Git-An-Example-Git-Enforced-Policy#_an_example_git_enforced_policy
   - :ref:`python_pre_commit_tool`




Server-Side Hooks
===================


In addition to the client-side hooks, you can use a couple of important
server-side hooks as a system administrator to enforce nearly any kind
of policy for your project.

These scripts run before and after pushes to the server.

The pre hooks can exit non-zero at any time to reject the push as well
as print an error message back to the client; you can set up a push
policy that’s as complex as you wish.

pre-receive
--------------

The first script to run when handling a push from a client is pre-receive.

It takes a list of references that are being pushed from stdin; if it
exits non-zero, none of them are accepted. You can use this hook to do
things like make sure none of the updated references are non-fast-forwards,
or to do access control for all the refs and files they’re modifying
with the push.


update
--------

The update script is very similar to the pre-receive script, except
that it’s run once for each branch the pusher is trying to update.
If the pusher is trying to push to multiple branches, pre-receive
runs only once, whereas update runs once per branch they’re pushing to.

Instead of reading from stdin, this script takes three arguments: the
name of the reference (branch), the SHA-1 that reference pointed to
before the push, and the SHA-1 the user is trying to push.

If the update script exits non-zero, only that reference is rejected;
other references can still be updated.

post-receive
---------------

The post-receive hook runs after the entire process is completed and
can be used to update other services or notify users. It takes the same
stdin data as the pre-receive hook.

Examples include emailing a list, notifying a **continuous integration server**,
or updating a ticket-tracking system – you can even parse the commit messages
to see if any tickets need to be opened, modified, or closed.

This script can’t stop the push process, but the client doesn’t disconnect
until it has completed, so be careful if you try to do anything that
may take a long time.
