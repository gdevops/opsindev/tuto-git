
.. index::
   pair: Git ; Hooks


.. _git_hooks:

=====================
Git hooks
=====================

.. seealso::

   - https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
   - https://pre-commit.com/hooks.html
   - :ref:`python_pre_commit_tool`




Description
==============

Like many other Version Control Systems, Git has a way to fire off
custom scripts when certain important actions occur.

There are two groups of these hooks:

- client-side
- and server-side.

Client-side hooks are triggered by operations such as committing and
merging, while server-side hooks run on network operations such as
receiving pushed commits.

You can use these hooks for all sorts of reasons.

Installing a Hook
====================

The hooks are all stored in the hooks subdirectory of the Git directory.

In most projects, that’s .git/hooks. When you initialize a new repository
with git init, Git populates the hooks directory with a bunch of
example scripts, many of which are useful by themselves; but they also
document the input values of each script.

All the examples are written as shell scripts, with some Perl thrown in,
but any properly named executable scripts will work fine – you can write
them in Ruby or Python or whatever language you are familiar with.

If you want to use the bundled hook scripts, you’ll have to rename them;
their file names all end with .sample.

To enable a hook script, put a file in the hooks subdirectory of your
.git directory that is named appropriately (without any extension) and
is executable.

From that point forward, it should be called.
We’ll cover most of the major hook filenames here_.


.. _here: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks


Client and server side hooks
===============================

.. toctree::
   :maxdepth: 3

   client_side/client_side
   server_side/server_side
