
.. index::
   pair: Git ; Client-side Hooks


.. _client_git_hooks:

=======================
Client-side git hooks
=======================

.. seealso::

   - https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
   - https://git-scm.com/book/en/v2/Customizing-Git-An-Example-Git-Enforced-Policy#_an_example_git_enforced_policy
   - :ref:`python_pre_commit_tool`





Client-Side Hooks
====================

There are a lot of client-side hooks.

This section splits them into committing-workflow hooks, email-workflow
scripts, and everything else.


.. note::  It’s important to note that client-side hooks are not copied
   when you clone a repository.
   If your intent with these scripts is to enforce a policy, you’ll
   probably want to do that on the server side; see the example in
   `An Example Git-Enforced Policy`_.

.. _`An Example Git-Enforced Policy`:  https://git-scm.com/book/en/v2/Customizing-Git-An-Example-Git-Enforced-Policy#_an_example_git_enforced_policy

Committing-Workflow Hooks
============================

The first four hooks have to do with the committing process.


.. _pre_commit_hook:

pre-commit hook
------------------

The pre-commit hook is run first, before you even type in a commit message.

It’s used to inspect the snapshot that’s about to be committed, to see
if you’ve forgotten something, to make sure tests run, or to examine
whatever you need to inspect in the code.

Exiting non-zero from this hook aborts the commit, although you can
bypass it with::

    git commit --no-verify.

You can do things like check for code style (run lint or something
equivalent), check for :ref:`trailing whitespace <trailing_whitespace>`
(the default hook does exactly this), or check for appropriate documentation
on new methods.

Python pre-commit tool
+++++++++++++++++++++++++

.. seealso::

   - :ref:`python_pre_commit_tool`
