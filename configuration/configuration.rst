
.. index::
   pair: Git ; Configuration


.. _git_configuration:

===================
Configuration
===================

.. seealso::

   - https://delicious-insights.com/fr/articles/configuration-git/


.. toctree::
   :maxdepth: 3

   gitconfig/gitconfig
   gitignore/gitignore
   extensions/extensions
   git_hooks/git_hooks
   gitlab/gitlab
