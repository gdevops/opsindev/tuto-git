
.. index::
   pair: Git ; .gitignore


.. _git_ignore:

===================
.gitignore file
===================

.. seealso::

   - https://github.com/github/gitignore
