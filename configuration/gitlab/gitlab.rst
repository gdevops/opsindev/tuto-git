.. index::
   pair: SSH configuration ; gitlab
   !  gitlab SSH configuration

.. _gitlab_ssh_configuration:

=================================
**gitlab SSH** configuration
=================================


Description
=============

- https://docs.gitlab.com/ee/ssh/

GitLab uses the SSH protocol to securely communicate with Git.

When you use SSH keys to authenticate to the GitLab remote server, you
**don’t need to supply your username and password each time.**


Supported SSH key types
---------------------------

To communicate with GitLab, you can use the following SSH key types:

- ED25519
- RSA
- DSA (Deprecated in GitLab 11.0.)
- ECDSA (As noted in Practical Cryptography With Go, the security issues
  related to DSA also apply to ECDSA.)

ED25519 SSH keys
-------------------

The book Practical Cryptography With Go suggests that ED25519 keys are
more secure and performant than RSA keys.

OpenSSH 6.5 introduced ED25519 SSH keys in 2014 and they should be
available on most operating systems


SSH keys
===========

~/.ssh/config
---------------

cat ~/.ssh/config

::

    # noamsw@pm.me account
    Host gitlab.com-noamsw
       HostName gitlab.com
       PreferredAuthentications publickey
       IdentityFile ~/.ssh/id_ed25519_noamsw

    # pvergain@pm.me account
    Host gitlab.com-pvergain
       HostName gitlab.com
       PreferredAuthentications publickey
       IdentityFile ~/.ssh/id_ed25519_pvergain_pm_me


~/.ssh
----------

::

    tree -L 3 ~/.ssh
        /home/pvergain/.ssh
        ├── config
        ├── id_ed25519
        ├── id_ed25519_noamsw
        ├── id_ed25519_pvergain_pm_me
        └── known_hosts
